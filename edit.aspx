﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="Conference.edit" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="PdfViewer" Namespace="PdfViewer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>Article info</h2>

    <table style="width: 50%;" border="1">        
        <tr>
            <td class="input-large" style="width: 207px"><b>Section:</b></td>
            <td><asp:Label ID="Label2" runat="server" 
                    Text="Mechanics, mathematical modeling and information technologies in the oil and gas sector"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Keywords:</b></td>
            <td><asp:Label ID="Label3" runat="server" Text="iterative methods,modeling"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Submission date:</b></td>
            <td><asp:Label ID="Label4" runat="server" Text="01.02.2013"></asp:Label></td>
        </tr>
            
    </table>
    <br/>
    Abstract:
    <br/>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    
    <br/>
    <hr/>

    <h2>Evaluation</h2>
     <table border="1">
            <tr>
            <td>RELEVANCE</td>
            <td> 
                <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Does the abstract address topical areas requested for the conference?</span></td>
            </tr>
            <tr>
            <td>ACTUALITY </td><td>
                How much the issue of the abstract is currently actual?</td>
            </tr>
            <tr>
            <td>NOVELTY</td><td>
                <p class="ListParagraph" 
                    style="margin: 0px; padding-top: 0px; padding-bottom: 0.75em; color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;">
                    <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                    Does the abstract shows innovative laboratory or data analysis techniques, or 
                    presents a new topic or application?</span></p>
                </td>
            </tr>
            <tr>
            <td>QUALITY</td><td>
                <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Does the abstract is well-written and organized?</span><span 
                    style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">&nbsp;<span 
                    class="Apple-converted-space">&nbsp;Is t</span></span><span 
                    style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">he 
                scope of the work can be clearly understood?</span></td>
            </tr>
            <tr>
            <td >LANGUAGE</td><td>
                <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Is the abstract well written, grammatically correct?</span></td>
            </tr>
           
        </table>
        <br />
    <em>
        <a href="evacriteria.htm">Read more about paper evaluation criterias</a><br />
    <br />
    Marks:</em>
     <table border="1">
            <tr>
            <td style='Color:green'>5</td><td style='Color:green'>excellent</td><td> Accept (I 
                vote to accept this paper.)</td>
            </tr>
            <tr>
            <td style='Color:green'>4</td><td style='Color:green'>very good</td><td>Weak Accept 
                (I can support a vote to accept, but I won’t argue for it.)</td>
            </tr>
            <tr>
            <td style='Color:green'>3</td><td style='Color:green'>good</td><td>Neutral (I’m not 
                impressed but I don’t feel strongly for or against it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>2</td><td style='Color:red'>fair</td><td>Weak Reject (I don’t 
                like it, but I won’t vote to completely reject it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>1</td><td style='Color:red'>poor</td><td>&nbsp;Total Reject (I 
                vote to reject this paper.)</td>
            </tr>
        </table>
        &nbsp;<h3>&nbsp;</h3>
    <h3>Criteria:</h3>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table>
        <tr>
            <td class="input-small" style="width: 127px">RELEVANCE</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
    
        <tr>
            <td class="input-small" style="width: 127px">ACTUALITY </td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td class="input-small" style="width: 127px">NOVELTY</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList5" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  <asp:ListItem>2</asp:ListItem>  <asp:ListItem>3</asp:ListItem>  <asp:ListItem>4</asp:ListItem>  <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td class="input-small" style="width: 127px">QUALITY</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td class="input-small" style="width: 127px">LANGUAGE</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList7" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
    </table>
        <asp:Label ID="Label6" runat="server" Text="Average Mark: "></asp:Label>
        <asp:Label ID="Label5" runat="server" Text="0"></asp:Label>
        <br />
        <asp:Label ID="Label13" runat="server" Text="Comments"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Height="150px" 
            TextMode="MultiLine" Width="298px"></asp:TextBox>
        <br/>
    
   </ContentTemplate>    
   </asp:UpdatePanel>
   <asp:Button ID="Button1" runat="server" Text="Evaluate abstract" onclick="Button1_Click" />
    <br/>    
</asp:Content>
