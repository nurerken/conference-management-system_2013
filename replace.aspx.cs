﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Text;
using System.IO;

namespace Conference
{
    public partial class replace : System.Web.UI.Page
    {
        string type = ""; // 0 - abstract, 1 - paper, 2 - ppt
        string sub_id = "";

        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (Session["type"] != null && Session["sub_id"] != null)
                {
                    type = Session["type"].ToString();
                    sub_id = Session["sub_id"].ToString();

                    if (!IsPostBack) 
                    {
                        FileUpload1.Attributes.Add("onchange", "return checkFileExtension();");
                    }
                }
                else Response.Redirect("detail.aspx");
            }
            else Response.Redirect("default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           

            if (FileUpload1.HasFile)
            {
                string exttension = System.IO.Path.GetExtension(FileUpload1.FileName);

                if (exttension.Equals(".pdf"))
                {
                    try
                    {
                        string new_path = User.Identity.Name.ToString() + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(FileUpload1.FileName));

                        if (type.Equals("0"))
                        {
                            string oldpath = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["AbstractPath"].ToString();
                            FileUpload1.SaveAs(Server.MapPath("Abstracts") + "\\" + new_path);
                            sta.UpdateAbstractPath(new_path, Int32.Parse(sub_id));

                            FileInfo TheFile = new FileInfo(Server.MapPath("Abstracts") + "\\" + oldpath);
                            if (TheFile.Exists)
                            {
                                File.Delete(Server.MapPath("Abstracts") + "\\" + oldpath);
                            }
                        }
                        if (type.Equals("1"))
                        {
                            string oldpath = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PaperPath"].ToString();
                            FileUpload1.SaveAs(Server.MapPath("Papers") + "\\" + new_path);
                            sta.UpdatePaperPath(new_path, Int32.Parse(sub_id));

                            FileInfo TheFile = new FileInfo(Server.MapPath("Papers") + "\\" + oldpath);
                            if (TheFile.Exists)
                            {
                                File.Delete(Server.MapPath("Papers") + "\\" + oldpath);
                            }
                        }

                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Submitted');");
                        Response.Write("document.location.href='detail.aspx';");
                        Response.Write("</script>");
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    } 
                }
                else
                {
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('You are uploading wrong file");
                    Response.Write("</script>");
                }
            }
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'A' && str[i] <= 'z'))
                    sb.Append(str[i]);
            }

            return sb.ToString();
        }
    }
}