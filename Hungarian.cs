﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Conference
{
    public class Hungarian
    {
        public static int calls;
        public static bool[,] pairs;
        public static bool[,] pairs2;

        public static double findLargest(double[][] array) //Finds the largest element in a 2D array.
        {
            double largest = double.NegativeInfinity;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    if (array[i][j] > largest)
                    {
                        largest = array[i][j];
                    }
                }
            }

            return largest;
        }
        public static double[][] transpose(double[][] array) //Transposes a double[][] array.
        {
            double[][] transposedArray = RectangularArrays.ReturnRectangularDoubleArray(array[0].Length, array.Length);
            for (int i = 0; i < transposedArray.Length; i++)
            {
                for (int j = 0; j < transposedArray[i].Length; j++)
                {
                    transposedArray[i][j] = array[j][i];
                }
            }
            return transposedArray;
        }
        public static double[][] copyOf(double[][] original) //Copies all elements of an array to a new array.
        {
            double[][] copy = RectangularArrays.ReturnRectangularDoubleArray(original.Length, original[0].Length);
            for (int i = 0; i < original.Length; i++)
            {
                //Need to do it this way, otherwise it copies only memory location
                Array.Copy(original[i], 0, copy[i], 0, original[i].Length);
            }

            return copy;
        }
        public static double[][] copyToSquare(double[][] original, double padValue) //Creates a copy of an array, made square by padding the right or bottom.
        {
            int rows = original.Length;
            int cols = original[0].Length; //Assume we're given a rectangular array.
            double[][] result = null;

            if (rows == cols) //The matrix is already square.
            {
                result = copyOf(original);
            }
            else if (rows > cols) //Pad on some extra columns on the right.
            {
                result = RectangularArrays.ReturnRectangularDoubleArray(rows, rows);
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < rows; j++)
                    {
                        if (j >= cols) //Use the padValue to fill the right columns.
                        {
                            result[i][j] = padValue;
                        }
                        else
                        {
                            result[i][j] = original[i][j];
                        }
                    }
                }
            }
            else
            { // rows < cols; Pad on some extra rows at the bottom.
                result = RectangularArrays.ReturnRectangularDoubleArray(cols, cols);
                for (int i = 0; i < cols; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        if (i >= rows) //Use the padValue to fill the bottom rows.
                        {
                            result[i][j] = padValue;
                        }
                        else
                        {
                            result[i][j] = original[i][j];
                        }
                    }
                }
            }

            return result;
        }
        static string sumType = "max";
        
        //Core of the algorithm; takes required inputs and returns the assignments
        public static int[][] hgAlgorithmAssignments(double[][] array, string sumType)
        {
            double maxWeightPlusOne = findLargest(array) + 1;

            double[][] cost = copyToSquare(array, maxWeightPlusOne); //Create the cost matrix

            if (sumType.Equals("max", StringComparison.CurrentCultureIgnoreCase)) //Then array is a profit array.  Must flip the values because the algorithm finds lowest.
            {
                for (int i = 0; i < cost.Length; i++) //Generate profit by subtracting from some value larger than everything.
                {
                    for (int j = 0; j < cost[i].Length; j++)
                    {
                        cost[i][j] = (maxWeightPlusOne - cost[i][j]);
                    }
                }
            }

            int[][] mask = RectangularArrays.ReturnRectangularIntArray(cost.Length, cost[0].Length); //The mask array.
            int[] rowCover = new int[cost.Length]; //The row covering vector.
            int[] colCover = new int[cost[0].Length]; //The column covering vector.
            int[] zero_RC = new int[2]; //Position of last zero from Step 4.
            int[][] path = RectangularArrays.ReturnRectangularIntArray(cost.Length * cost[0].Length + 2, 2);
            int step = 1;
            bool done = false;
            while (done == false) //main execution loop
            {
                switch (step)
                {
                    case 1:
                        step = hg_step1(step, cost);
                        break;
                    case 2:
                        step = hg_step2(step, cost, mask, rowCover, colCover);
                        break;
                    case 3:
                        step = hg_step3(step, mask, colCover);
                        break;
                    case 4:
                        step = hg_step4(step, cost, mask, rowCover, colCover, zero_RC);
                        break;
                    case 5:
                        step = hg_step5(step, mask, rowCover, colCover, zero_RC, path);
                        break;
                    case 6:
                        step = hg_step6(step, cost, rowCover, colCover);
                        break;
                    case 7:
                        done = true;
                        break;
                    
                }
            } //end while

            int[][] assignments = RectangularArrays.ReturnRectangularIntArray(array.Length, 2); //Create the returned array.
            int assignmentCount = 0; //In a input matrix taller than it is wide, the first
        
            for (int i = 0; i < mask.Length; i++)
            {
                for (int j = 0; j < mask[i].Length; j++)
                {
                    if (i < array.Length && j < array[0].Length && mask[i][j] == 1)
                    {
                        if (true)
                        {
                            assignments[assignmentCount][0] = i;
                            assignments[assignmentCount][1] = j;
                            assignmentCount++;
                        }
                    }
                }
            }
            return assignments;
        }
       
        public static double hgAlgorithm(double[][] array, bool[,] prs, bool[,] prs2, int cls)
        {
            calls = cls;
            pairs = prs;
            pairs2 = prs2;
            return getAssignmentSum(array, hgAlgorithmAssignments(array, sumType));
        }
        public static double getAssignmentSum(double[][] array, int[][] assignments)
        {
            double sum = 0;
            for (int i = 0; i < assignments.Length; i++)
            {
                sum = sum + array[assignments[i][0]][assignments[i][1]];
                if (calls == 1)
                {
                    pairs[assignments[i][0], assignments[i][1]] = true;
                    //array[assignments[i][0]][assignments[i][1]] = Double.NegativeInfinity+1;
                }
                if (calls == 2) pairs2[assignments[i][0], assignments[i][1]] = true;
            }
            return sum;
        }
        public static int hg_step1(int step, double[][] cost)
        {
            double minval;

            for (int i = 0; i < cost.Length; i++)
            {
                minval = cost[i][0];
                for (int j = 0; j < cost[i].Length; j++) //1st inner loop finds min val in row.
                {
                    if (minval > cost[i][j])
                    {
                        minval = cost[i][j];
                    }
                }
                for (int j = 0; j < cost[i].Length; j++) //2nd inner loop subtracts it.
                {
                    cost[i][j] = cost[i][j] - minval;
                }
            }

            step = 2;
            return step;
        }
        public static int hg_step2(int step, double[][] cost, int[][] mask, int[] rowCover, int[] colCover)
        {
            for (int i = 0; i < cost.Length; i++)
            {
                for (int j = 0; j < cost[i].Length; j++)
                {
                    if ((cost[i][j] == 0) && (colCover[j] == 0) && (rowCover[i] == 0))
                    {
                        mask[i][j] = 1;
                        colCover[j] = 1;
                        rowCover[i] = 1;
                    }
                }
            }

            clearCovers(rowCover, colCover); //Reset cover vectors.

            step = 3;
            return step;
        }
        public static int hg_step3(int step, int[][] mask, int[] colCover)
        {
            for (int i = 0; i < mask.Length; i++) //Cover columns of starred zeros.
            {
                for (int j = 0; j < mask[i].Length; j++)
                {
                    if (mask[i][j] == 1)
                    {
                        colCover[j] = 1;
                    }
                }
            }

            int count = 0;
            for (int j = 0; j < colCover.Length; j++) //Check if all columns are covered.
            {
                count = count + colCover[j];
            }

            if (count >= mask.Length) //Should be cost.length but ok, because mask has same dimensions.
            {
                step = 7;
            }
            else
            {
                step = 4;
            }

            return step;
        }
        public static int hg_step4(int step, double[][] cost, int[][] mask, int[] rowCover, int[] colCover, int[] zero_RC)
        {
            int[] row_col = new int[2]; //Holds row and col of uncovered zero.
            bool done = false;
            while (done == false)
            {
                row_col = findUncoveredZero(row_col, cost, rowCover, colCover);
                if (row_col[0] == -1)
                {
                    done = true;
                    step = 6;
                }
                else
                {
                    mask[row_col[0]][row_col[1]] = 2; //Prime the found uncovered zero.

                    bool starInRow = false;
                    for (int j = 0; j < mask[row_col[0]].Length; j++)
                    {
                        if (mask[row_col[0]][j] == 1) //If there is a star in the same row...
                        {
                            starInRow = true;
                            row_col[1] = j; //remember its column.
                        }
                    }

                    if (starInRow == true)
                    {
                        rowCover[row_col[0]] = 1; //Cover the star's row.
                        colCover[row_col[1]] = 0; //Uncover its column.
                    }
                    else
                    {
                        zero_RC[0] = row_col[0]; //Save row of primed zero.
                        zero_RC[1] = row_col[1]; //Save column of primed zero.
                        done = true;
                        step = 5;
                    }
                }
            }

            return step;
        }
        public static int[] findUncoveredZero(int[] row_col, double[][] cost, int[] rowCover, int[] colCover) //Aux 1 for hg_step4.
        {
            row_col[0] = -1; //Just a check value. Not a real index.
            row_col[1] = 0;

            int i = 0;
            bool done = false;
            while (done == false)
            {
                int j = 0;
                while (j < cost[i].Length)
                {
                    if (cost[i][j] == 0 && rowCover[i] == 0 && colCover[j] == 0)
                    {
                        row_col[0] = i;
                        row_col[1] = j;
                        done = true;
                    }
                    j = j + 1;
                } //end inner while
                i = i + 1;
                if (i >= cost.Length)
                {
                    done = true;
                }
            } //end outer while

            return row_col;
        }
        public static int hg_step5(int step, int[][] mask, int[] rowCover, int[] colCover, int[] zero_RC, int[][] path)
        {
           
            int count = 0; //Counts rows of the path matrix.
            //int[][] path = new int[(mask[0].length + 2)][2];	//Path matrix (stores row and col).
            path[count][0] = zero_RC[0]; //Row of last prime.
            path[count][1] = zero_RC[1]; //Column of last prime.

            bool done = false;
            while (done == false)
            {
                int r = findStarInCol(mask, path[count][1]);
                if (r >= 0)
                {
                    count = count + 1;
                    path[count][0] = r; //Row of starred zero.
                    path[count][1] = path[count - 1][1]; //Column of starred zero.
                }
                else
                {
                    done = true;
                }

                if (done == false)
                {
                    int c = findPrimeInRow(mask, path[count][0]);
                    count = count + 1;
                    path[count][0] = path[count - 1][0]; //Row of primed zero.
                    path[count][1] = c; //Col of primed zero.
                }
            } //end while

            convertPath(mask, path, count);
            clearCovers(rowCover, colCover);
            erasePrimes(mask);

            step = 3;
            return step;

        }
        public static int findStarInCol(int[][] mask, int col) //Aux 1 for hg_step5.
        {
            int r = -1; //Again this is a check value.
            for (int i = 0; i < mask.Length; i++)
            {
                if (mask[i][col] == 1)
                {
                    r = i;
                }
            }

            return r;
        }
        public static int findPrimeInRow(int[][] mask, int row) //Aux 2 for hg_step5.
        {
            int c = -1;
            for (int j = 0; j < mask[row].Length; j++)
            {
                if (mask[row][j] == 2)
                {
                    c = j;
                }
            }

            return c;
        }
        public static void convertPath(int[][] mask, int[][] path, int count) //Aux 3 for hg_step5.
        {
            for (int i = 0; i <= count; i++)
            {
                if (mask[path[i][0]][path[i][1]] == 1)
                {
                    mask[path[i][0]][path[i][1]] = 0;
                }
                else
                {
                    mask[path[i][0]][path[i][1]] = 1;
                }
            }
        }
        public static void erasePrimes(int[][] mask) //Aux 4 for hg_step5.
        {
            for (int i = 0; i < mask.Length; i++)
            {
                for (int j = 0; j < mask[i].Length; j++)
                {
                    if (mask[i][j] == 2)
                    {
                        mask[i][j] = 0;
                    }
                }
            }
        }
        public static void clearCovers(int[] rowCover, int[] colCover) //Aux 5 for hg_step5 (and not only).
        {
            for (int i = 0; i < rowCover.Length; i++)
            {
                rowCover[i] = 0;
            }
            for (int j = 0; j < colCover.Length; j++)
            {
                colCover[j] = 0;
            }
        }
        public static int hg_step6(int step, double[][] cost, int[] rowCover, int[] colCover)
        {
            double minval = findSmallest(cost, rowCover, colCover);

            for (int i = 0; i < rowCover.Length; i++)
            {
                for (int j = 0; j < colCover.Length; j++)
                {
                    if (rowCover[i] == 1)
                    {
                        cost[i][j] = cost[i][j] + minval;
                    }
                    if (colCover[j] == 0)
                    {
                        cost[i][j] = cost[i][j] - minval;
                    }
                }
            }

            step = 4;
            return step;
        }
        public static double findSmallest(double[][] cost, int[] rowCover, int[] colCover) //Aux 1 for hg_step6.
        {
            double minval = double.PositiveInfinity; //There cannot be a larger cost than this.
            for (int i = 0; i < cost.Length; i++) //Now find the smallest uncovered value.
            {
                for (int j = 0; j < cost[i].Length; j++)
                {
                    if (rowCover[i] == 0 && colCover[j] == 0 && (minval > cost[i][j]))
                    {
                        minval = cost[i][j];
                    }
                }
            }

            return minval;
        }
    }
}
