﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Confirm.aspx.cs" Inherits="Conference.Confirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>Article info</h2>

    <table style="width: 50%;" border="1">        
        <tr>
            <td class="input-large" style="width: 207px"><b>Section:</b></td>
            <td>
                <asp:Label ID="Label1" runat="server" 
                    Text="Research on iterational method to solve SLAE"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Topic:</b></td>
            <td><asp:Label ID="Label2" runat="server" 
                    Text="Mechanics, mathematical modeling and information technologies in the oil and gas sector"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Keywords:</b></td>
            <td><asp:Label ID="Label3" runat="server" Text="iterative methods,modeling"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Submission date:</b></td>
            <td><asp:Label ID="Label4" runat="server" Text="01.02.2013"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Author:</b></td>
            <td><asp:Label ID="Label5" runat="server" Text="Kenzhebekov Nurlan"></asp:Label></td>
        </tr>

        <tr>
            <td class="input-large" style="width: 207px"><b>email:</b></td>
            <td><asp:Label ID="Label14" runat="server" Text="Kenzhebekov Nurlan"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Organization:</b></td>
            <td><asp:Label ID="Label15" runat="server" Text="Kenzhebekov Nurlan"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Job:</b></td>
            <td><asp:Label ID="Label16" runat="server" Text="Kenzhebekov Nurlan"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Country:</b></td>
            <td><asp:Label ID="Label17" runat="server" Text="Kenzhebekov Nurlan"></asp:Label></td>
        </tr>


        <tr>
            <td class="input-large" style="width: 207px"><b>Status:</b></td>
            <td><asp:Label ID="Label6" runat="server" Text="Under evaluating process"></asp:Label></td>
        </tr>
        
    </table>

    <br/>
    <p>Abstract</p>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    <br/> 
    <br/> 

    <table border="1">
        <tr>
            <td><b>Reviewer</b></td>
            <td><b>Mark</b></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></td><td><asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></td><td><asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label11" runat="server" Text="Average Mark"></asp:Label></td><td><asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></td>
        </tr>
    </table>
    <asp:Label ID="Label13" runat="server" Text="Comments"></asp:Label>
    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" 
        Height="168px" Width="310px"></asp:TextBox>
    <br />
        <asp:Button ID="Button3" runat="server" Text="Notify Author" 
        onclick="Button3_Click" />
        <br/>
    <asp:Button ID="Button1" runat="server" Text="Accept" 
        onclick="Button1_Click1" /><asp:Button ID="Button2" runat="server" 
        Text="Decline" onclick="Button2_Click1" />
</asp:Content>
