﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="edit2.aspx.cs" Inherits="Conference.edit2" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="PdfViewer" Namespace="PdfViewer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>Article info</h2>

    <table style="width: 50%;" border="1">        

        <tr>
            <td class="input-large" style="width: 207px"><b>Section:</b></td>
            <td><asp:Label ID="Label2" runat="server" 
                    Text="Mechanics, mathematical modeling and information technologies in the oil and gas sector"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Keywords:</b></td>
            <td><asp:Label ID="Label3" runat="server" Text="iterative methods,modeling"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Submission date:</b></td>
            <td><asp:Label ID="Label4" runat="server" Text="01.02.2013"></asp:Label></td>
        </tr>
               
    </table>
    <br/>
    Paper:
    <br/>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    
    <br/>
    <hr/>
    
    <h2>Evaluation</h2>
    <em>
    <br />
    Criteria:</em><br />
     <table border="1">
            <tr>
            <td>COVERAGE</td>
            <td> 
                How much
                <span style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                the article adequately cover its topic. Does the article present original ideas or rehash those of others? Are significant aspects of the topic omitted?Are omissions acknowledged and explained?</span>
                </td>
            </tr>
            <tr>
            <td>OBJECTIVITY</td><td>
                <p class="ListParagraph" 
                    style="margin: 0px; padding-top: 0px; padding-bottom: 0.75em; color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;">
                    <span style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                    Is the information biased or balanced, subjective or objective?Is the text 
                    mostly fact or opinion? Is that appropriate? Does the text acknowledge the 
                    above? Does the writer use logical or emotional appeal?</span></p>
                </td>
            </tr>
            <tr>
            <td>ADVANCEMENT OF FIELD</td><td>
                <p class="ListParagraph" 
                    style="margin: 0px; padding-top: 0px; padding-bottom: 0.75em; color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;">
                    Does paper present a significant contribution to the field?</p>
                </td>
            </tr>
            <tr>
            <td>ACCURACY &amp; RELIABILITY</td><td>
                <span style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Is a bibliography or reference list available so information can be verified? 
                Does the article offer trustworthy information? Is the information protected by 
                copyright? Who is the copyright holder? Does the article indicate editorial 
                quality (free of errors)?</span></td>
            </tr>
            <tr>
            <td >CURRENCY</td><td>
                <span style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                When was the article published? Does that matter? Is the information current? 
                Should it be?</span><br 
                    style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" />
                <span style="color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Are current research findings and/or theories evident? Should they be?</span></td>
            </tr>
            <tr>
            <td>STATE OF COMPLETION</td><td>
                <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19.03125px; orphans: auto; text-align: left; text-indent: 47.266666412353516px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
                Does abstract show substantial results indicating that the work has been, or is 
                nearly completed?</span></td>
            </tr>
        </table>
        <a href="evacriteria.htm">Read more about paper evaluation criterias</a><br />
    <em>
    <br />
    Marks:</em>
     <table border="1">
            <tr>
            <td style='Color:green'>5</td><td style='Color:green'>excellent</td><td> Accept (I 
                vote to accept this paper.)</td>
            </tr>
            <tr>
            <td style='Color:green'>4</td><td style='Color:green'>very good</td><td>Weak Accept 
                (I can support a vote to accept, but I won’t argue for it.)</td>
            </tr>
            <tr>
            <td style='Color:green'>3</td><td style='Color:green'>good</td><td>Neutral (I’m not 
                impressed but I don’t feel strongly for or against it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>2</td><td style='Color:red'>fair</td><td>Weak Reject (I don’t 
                like it, but I won’t vote to completely reject it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>1</td><td style='Color:red'>poor</td><td>Total Reject (I 
                vote to reject this paper.)</td>
            </tr>
        </table>
        &nbsp;<h3>Criteria:</h3>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table>
        <tr>
            <td>COVERAGE</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
     
     <tr>
            <td>OBJECTIVITY</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>

        <tr>
            <td>ADVANCEMENT OF FIELD</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td>ACCURACY &amp; RELIABILITY</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList5" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  <asp:ListItem>2</asp:ListItem>  <asp:ListItem>3</asp:ListItem>  <asp:ListItem>4</asp:ListItem>  <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td>CURRENCY</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
            <td>STATE OF COMPLETION</td>
            <td> 
            <asp:RadioButtonList ID="RadioButtonList7" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="changed1" AutoPostBack="true">  
                    <asp:ListItem>1</asp:ListItem>  
                    <asp:ListItem>2</asp:ListItem>  
                    <asp:ListItem>3</asp:ListItem>  
                    <asp:ListItem>4</asp:ListItem>  
                    <asp:ListItem>5</asp:ListItem>  
                </asp:RadioButtonList>
             </td>
        </tr>
    </table>
        <asp:Label ID="Label6" runat="server" Text="Average Mark: "></asp:Label>
        <asp:Label ID="Label5" runat="server" Text="0"></asp:Label>
        <br />
        <asp:Label ID="Label13" runat="server" Text="Comments"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Height="150px" ReadOnly="false" 
            TextMode="MultiLine" Width="298px"></asp:TextBox>
        <br/>
    <br/>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="Button1" runat="server" Text="Evaluate Article" onclick="Button1_Click" />
</asp:Content>
