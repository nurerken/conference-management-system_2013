﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;

namespace Conference
{
    public partial class MyProfile : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string userID = "";
        string userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID=User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();

                if (userType.Equals("0") || userType.Equals("1"))
                {
                    if (!IsPostBack)
                    {
                        Label1.Text = uta.GetDataByUserID(userID).Rows[0]["FIO"].ToString();
                        Label3.Text = uta.GetDataByUserID(userID).Rows[0]["Organization"].ToString();
                        Label4.Text = uta.GetDataByUserID(userID).Rows[0]["Job"].ToString();
                        Label5.Text = uta.GetDataByUserID(userID).Rows[0]["Country"].ToString();
                        Label7.Text = uta.GetDataByUserID(userID).Rows[0]["Email"].ToString();
                        Label2.Text = uta.GetDataByUserID(userID).Rows[0]["Accompanying"].ToString();
                        
                        GridView1.DataSource = sta.GetSubmissionsForAuthor(userID);
                        GridView1.DataBind();

                        //Response.Write((uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString()));
                        if (Int32.Parse(uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString()) == 0) 
                        {
                            GridView1.Visible = false;
                        }
                        if (Int32.Parse(uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString()) == 1)
                        {
                            Label6.Visible = false;
                        }
                        if (Int32.Parse(uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString()) >= 2)
                        {
                            Label6.Visible = false;
                            Button1.Visible = false;
                        }
                        
                        DateTime deadline = Convert.ToDateTime(cta.GetData().Rows[0]["ArticleSubDeadline"].ToString());
                        if (System.DateTime.Today > deadline)
                        {
                            Button1.Visible = false;
                        }
                    }
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }

        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'> Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }

        protected string ReturnDate(object val)
        {
                return val.ToString().Substring(0, 10);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int subs = Int32.Parse(uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString());
            
            if(subs==1) Response.Redirect("submission2.aspx");
            else Response.Redirect("submission.aspx");
        }

        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/detail.aspx");
        }

        protected void delete(object sender, EventArgs e)
        {
            string sid = (sender as Button).CommandArgument.ToString();
            sta.DeleteQuery(Convert.ToInt32(sid));
            int oldsubs = Convert.ToInt32(uta.GetDataByUserID(userID).Rows[0]["submissions"].ToString());
            oldsubs--;
            uta.UpdateSubmissionNumber(oldsubs.ToString(), userID);
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Deleted');");
            Response.Write("document.location.href='myprofile.aspx';");
            Response.Write("</script>");  
        }
    }
}