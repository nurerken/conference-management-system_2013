﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="Conference.CreateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="Label1" runat="server" Text="CREATE REVIEWER" Font-Bold="True"></asp:Label>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table>
        <tr>
            <td>User ID</td><td><asp:TextBox ID="TextBox1" runat="server" Width="216px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Name</td><td><asp:TextBox ID="TextBox2" runat="server" Width="216px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Email</td><td><asp:TextBox ID="TextBox3" runat="server" Width="216px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Organization</td><td><asp:TextBox ID="TextBox4" runat="server" 
                Width="216px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Job</td><td><asp:TextBox ID="TextBox5" runat="server" Width="219px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Country</td><td>
            <asp:DropDownList ID="DropDownList2" runat="server">
            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Topic Area</td><td><asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="changed">
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td>Keyword</td><td>
            <asp:CheckBoxList ID="ckeckboxlist1" runat="server" AutoPostBack="True" 
                Font-Bold="True" ForeColor="#003300"                 
                RepeatDirection="Vertical">
            </asp:CheckBoxList>
            </td>
        </tr>
    </table>
    
    </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button ID="Button1" runat="server" Text="Create" onclick="Button1_Click" />
</asp:Content>
