﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Conference.DataSet1TableAdapters;
using System.IO;

namespace Conference
{
    public partial class Schedule : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        sectionsTableAdapter secta = new sectionsTableAdapter();
        string userID, userType;
        static DateTime startdate;
        int session = 0;
        string mysession = "";
        //bool programCreated = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (true)
            {
                //userID = User.Identity.Name.ToString();
                //userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                if (true)
                {
                    if (!IsPostBack)
                    {
                        startdate = Convert.ToDateTime(cta.GetData().Rows[0]["StartDate"].ToString());
                        //if (cta.GetData().Rows[0]["ProgramCreated"].ToString().Equals("1")) programCreated = true;
                        if (Session["mysession"] != null) mysession = Session["mysession"].ToString();
                        generate();
                    }
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }

        void generate() 
        {
            Label label0 = new Label();
            label0.Text = "<h3><b>Important Events</b></h3>";
            Panel1.Controls.Add(label0);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            Table table = new Table();
            TableRow tRow = new TableRow();
            tRow.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tRow);

            TableCell tc4a = new TableCell();
            tc4a.Text = "<b>TIME</b>";
            tc4a.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc4a);

            TableCell tc5 = new TableCell();
            tc5.Text = "<b>EVENT</b>";
            tc5.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc5);

            TableCell t43421 = new TableCell();
            t43421.Text = "<b>Place</b>";
            t43421.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(t43421);

            TableRow tRow2 = new TableRow();
            tRow2.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tRow2);
            TableCell tcc = new TableCell();
            tcc.Text = "<b>"+startdate.ToString().Substring(0,10)+"<br/> 8:30-9:30</b>";
            tcc.Attributes.Add("style", "border:1px solid black;");
            tRow2.Cells.Add(tcc);
            TableCell tcc2 = new TableCell();
            tcc2.Text = "<b>Retistration</b>";
            tcc2.Attributes.Add("style", "border:1px solid black;");
            tRow2.Cells.Add(tcc2);
            TableCell tcc3 = new TableCell();
            tcc3.Text = "<b>Main Hall</b>";
            tcc3.Attributes.Add("style", "border:1px solid black;");
            tRow2.Cells.Add(tcc3);

            TableRow tRoww = new TableRow();
            tRoww.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tRoww);
            TableCell tccw = new TableCell();
            tccw.Text = "<b>" + startdate.ToString().Substring(0, 10) + "<br/> 9:30-10:30</b>";
            tccw.Attributes.Add("style", "border:1px solid black;");
            tRoww.Cells.Add(tccw);
            TableCell tccw2 = new TableCell();
            tccw2.Text = "<b>Opening Ceremony</b>";
            tccw2.Attributes.Add("style", "border:1px solid black;");
            tRoww.Cells.Add(tccw2);
            TableCell tccw3 = new TableCell();
            tccw3.Text = "<b>Main Hall</b>";
            tccw3.Attributes.Add("style", "border:1px solid black;");
            tRoww.Cells.Add(tccw3);

            TableRow tr = new TableRow();
            tr.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tr);
            TableCell tc = new TableCell();
            tc.Text = "<b>" + " 10:30-11:00</b>";
            tc.Attributes.Add("style", "border:1px solid black;");
            tr.Cells.Add(tc);
            TableCell tc22 = new TableCell();
            tc22.Text = "<b>coffee break</b>";
            tc22.Attributes.Add("style", "border:1px solid black;");
            tr.Cells.Add(tc22);
            TableCell tc33 = new TableCell();
            tc33.Text = "<b>Canteen</b>";
            tc33.Attributes.Add("style", "border:1px solid black;");
            tr.Cells.Add(tc33);

            TableRow tr2 = new TableRow();
            tr2.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tr2);
            TableCell ttc = new TableCell();
            ttc.Text = "<b>" + " 11.00-14.00</b>";
            ttc.Attributes.Add("style", "border:1px solid black;");
            tr2.Cells.Add(ttc);
            TableCell tccx= new TableCell();
            tccx.Text = "<b>Presentations</b>";
            tccx.Attributes.Add("style", "border:1px solid black;");
            tr2.Cells.Add(tccx);
            TableCell tcc33 = new TableCell();
            tcc33.Text = "<b>Halls</b>";
            tcc33.Attributes.Add("style", "border:1px solid black;");
            tr2.Cells.Add(tcc33);

            TableRow tr52 = new TableRow();
            tr52.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tr52);
            TableCell ttc2 = new TableCell();
            ttc2.Text = "<b>" + " 14.00-15.00</b>";
            ttc2.Attributes.Add("style", "border:1px solid black;");
            tr52.Cells.Add(ttc2);
            TableCell ttc22 = new TableCell();
            ttc22.Text = "<b>Lunch break</b>";
            ttc22.Attributes.Add("style", "border:1px solid black;");
            tr52.Cells.Add(ttc22);
            TableCell ttc222 = new TableCell();
            ttc222.Text = "<b>Canteen</b>";
            ttc222.Attributes.Add("style", "border:1px solid black;");
            tr52.Cells.Add(ttc222);

            TableRow tri = new TableRow();
            tri.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tri);
            TableCell tco = new TableCell();
            tco.Text = "<b>" + " 15.00-18.00</b>";
            tco.Attributes.Add("style", "border:1px solid black;");
            tri.Cells.Add(tco);
            TableCell tcox = new TableCell();
            tcox.Text = "<b>Presentations</b>";
            tcox.Attributes.Add("style", "border:1px solid black;");
            tri.Cells.Add(tcox);
            TableCell tcoxe = new TableCell();
            tcoxe.Text = "<b>Halls</b>";
            tcoxe.Attributes.Add("style", "border:1px solid black;");
            tri.Cells.Add(tcoxe);

            TableRow trids = new TableRow();
            trids.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tri);
            TableCell tcui = new TableCell();
            tcui.Text = "<b>" + startdate.AddDays(2).ToString().Substring(0, 10) + " 11.00-15.00</b>";
            tcui.Attributes.Add("style", "border:1px solid black;");
            trids.Cells.Add(tcui);
            TableCell tcui2 = new TableCell();
            tcui2.Text = "<b>Free tour </b>";
            tcui2.Attributes.Add("style", "border:1px solid black;");
            trids.Cells.Add(tcui2);
            TableCell tcui22 = new TableCell();
            tcui22.Text = "<b>Historical Places</b>";
            tcui22.Attributes.Add("style", "border:1px solid black;");
            trids.Cells.Add(tcui22);

            Panel1.Controls.Add(table);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            Label label1 = new Label();
            label1.Text = "<h2><b>Schedule</b></h2>";
            Panel1.Controls.Add(label1);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            Label labeld1 = new Label();
            labeld1.Text = "<h1><b>Oral Presentations</b></h1>";
            Panel1.Controls.Add(labeld1);
            Panel1.Controls.Add(new LiteralControl("<br/>"));
            
            //for each topic create table
            int k = secta.GetData().Rows.Count;

            for (int i = 0; i < k; i++)
            {
                createTable(secta.GetData().Rows[i]["Title"].ToString(), "s" + i);
            }            

            //
            Label labelx = new Label();
            labelx.Text = "<h1><b>Virtual Presentations</b></h1>";
            Panel1.Controls.Add(labelx);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            for (int i = 0; i < k; i++)
            {
                createVirtualPresentations(secta.GetData().Rows[i]["Title"].ToString());
            }   
        }
        void createVirtualPresentations(string topic) 
        {
            int n = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "1").Rows.Count;//virtual speakers

            Label label1 = new Label();
            label1.Text = "<b>Section: " + topic + "</b>";
            Panel1.Controls.Add(label1);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            Table table = new Table();
            TableRow tRow = new TableRow();
            tRow.Attributes.Add("style", "border:1px solid black;");
            table.Rows.Add(tRow);           
            TableCell tc2 = new TableCell();
            tc2.Text = "<b>Author(s)</b>";
            tc2.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc2);
            TableCell tc3 = new TableCell();
            tc3.Text = "<b>Topic</b>";
            tc3.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc3);
            TableCell tc4 = new TableCell();
            tc4.Text = "<b>Date</b>";
            tc4.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc4);
            TableCell tc5 = new TableCell();
            tc5.Text = "<b>Time</b>";
            tc5.Attributes.Add("style", "border:1px solid black;");
            tRow.Cells.Add(tc5);            

            for (int i = 0; i < n; i++)
            {
                TableRow trow = new TableRow();
                trow.Attributes.Add("style", "border:1px solid black;");
                table.Rows.Add(trow);

                TableCell tcell2 = new TableCell();
                tcell2.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic,"1").Rows[i]["FIO"].ToString();
                tcell2.Attributes.Add("style", "border:1px solid black;");
                trow.Cells.Add(tcell2);
                TableCell tcell3 = new TableCell();
                tcell3.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic,"1").Rows[i]["Title"].ToString();
                tcell3.Attributes.Add("style", "border:1px solid black;");
                trow.Cells.Add(tcell3);
                TableCell tcell4 = new TableCell();
                DateTime datetime = startdate.AddDays(double.Parse(DayandTime(i)[0]));
                tcell4.Text = datetime.ToString().Substring(0, 10);
                tcell4.Attributes.Add("style", "border:1px solid black;");
                trow.Cells.Add(tcell4);
                TableCell tcell5 = new TableCell();
                tcell5.Text = DayandTime(i)[1];
                tcell5.Attributes.Add("style", "border:1px solid black;");
                trow.Cells.Add(tcell5);                
            }

            table.Attributes.Add("style", "border:1px solid black; background:#87cefa;");
            Panel1.Controls.Add(table);
            Panel1.Controls.Add(new LiteralControl("<hr/><br/>"));
        }
        int hours(int start, int dt)
        {
            return start + dt / 60;
        }
        int min(int dt)
        {
            return dt % 60;
        }
        string[] DayandTime(int i)// i = 0...n-1
        {
            string[] returndata = new string[2];// day, time
            int day = i / 30;

            if (i % 30 < 15)
            {
                int dt = (i % 30) * 20;
                int h1 = hours(9, dt);
                int min1 = min(dt);
                int h2 = hours(9, dt + 20);
                int min2 = min(dt + 20);

                returndata[0] = day.ToString();
                string time = h1.ToString() + "." + minutes(min1) + "-" + h2.ToString() + "." + minutes(min2);
                returndata[1] = time;
            }
            else if (i % 30 >= 15)
            {
                int dt = ((i - 15) % 30) * 20;
                int h1 = hours(15, dt);
                int min1 = min(dt);
                int h2 = hours(15, dt + 20);
                int min2 = min(dt + 20);

                returndata[0] = day.ToString();
                string time = h1.ToString() + "." + minutes(min1) + "-" + h2.ToString() + "." + minutes(min2);
                returndata[1] = time;
            }

            return returndata;
        }
        void createTable(string topic, string section)
        {
            Label label1 = new Label();
            label1.Text = "<h4>Section: " + topic + "</h4>";
            Panel1.Controls.Add(label1);
            Panel1.Controls.Add(new LiteralControl("<br/>"));

            int n = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic,"0").Rows.Count;//is not virtual speakers
            int m = upperbound(n,36);//rooms
            //Response.Write(n+";      "+m + "<br/>");
            System.Diagnostics.Debug.Write(topic+"; "+n);

            int cnt = 0;
            
            for (int i = 0; i <= 1; i++) //days
            {
                if (cnt >= n) break;
                Label labelx = new Label();
                labelx.Text = "<b>Day: " + (startdate.AddDays(i)).ToString().Substring(0,10) + "</b>";
                Panel1.Controls.Add(labelx);
                Panel1.Controls.Add(new LiteralControl("<br/><br/>"));

                for (int j = 0; j < m; j++) //rooms
                {
                    Label labelm = new Label();
                    labelm.Text = "<b>Session:" + (session + 1).ToString() + "     11.00-14.00" + "</b>";
                    Panel1.Controls.Add(labelm);
                    Panel1.Controls.Add(new LiteralControl("<br/><br/>"));
                    
                    Label label2 = new Label();
                    int x = (j+1);
                    label2.Text = "<b>Hall: " + x + "_" + section + "</b>";
                    Panel1.Controls.Add(label2);
                    Panel1.Controls.Add(new LiteralControl("<br/>"));
                    
                    Table table = new Table();
                    TableRow tRow = new TableRow();
                    tRow.Attributes.Add("style", "border:1px solid black;");
                    table.Rows.Add(tRow);
                    TableCell tc4 = new TableCell();
                    tc4.Text = "<b>Date</b>";
                    tc4.Attributes.Add("style", "border:1px solid black;");
                    tRow.Cells.Add(tc4);
                    TableCell tc5 = new TableCell();
                    tc5.Text = "<b>Time</b>";
                    tc5.Attributes.Add("style", "border:1px solid black;");
                    tRow.Cells.Add(tc5);
                    TableCell tc2 = new TableCell();
                    tc2.Text = "<b>Presenter(s)</b>";
                    tc2.Attributes.Add("style", "border:1px solid black;");
                    tRow.Cells.Add(tc2);
                    TableCell tc3 = new TableCell();
                    tc3.Text = "<b>Topic</b>";
                    tc3.Attributes.Add("style", "border:1px solid black;");
                    tRow.Cells.Add(tc3);         
                    
                    //create folder named session+1
                    if (mysession.Equals("Nurlan")) System.IO.Directory.CreateDirectory(Server.MapPath("GroupedPresentations") + "\\" + "Session_" + (session + 1).ToString());
                    
                    for (int k = 0; k < 9; k++)//time
                    {
                        if (cnt < n)
                        {
                            TableRow trow = new TableRow();
                            trow.Attributes.Add("style", "border:1px solid black;");
                            table.Rows.Add(trow);

                            TableCell tcell5 = new TableCell();
                            tcell5.Text = startdate.AddDays(i).ToString().Substring(0, 10);
                            tcell5.Attributes.Add("style", "border:1px solid black;");
                            trow.Cells.Add(tcell5);

                            TableCell tcell4 = new TableCell();
                            tcell4.Text = Time(k, 11);
                            tcell4.Attributes.Add("style", "border:1px solid black;");
                            trow.Cells.Add(tcell4);

                            TableCell tcell2 = new TableCell();
                            tcell2.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic,"0").Rows[cnt]["FIO"].ToString();
                            tcell2.Attributes.Add("style", "border:1px solid black;");
                            trow.Cells.Add(tcell2);

                            TableCell tcell3 = new TableCell();
                            tcell3.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "0").Rows[cnt]["Title"].ToString();
                            tcell3.Attributes.Add("style", "border:1px solid black;");
                            trow.Cells.Add(tcell3);

                            if (mysession.Equals("Nurlan"))
                            {
                                string oldpath = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "0").Rows[cnt]["PPTPath"].ToString();
                                copyFile(Server.MapPath("Presentations") + "\\" + oldpath, Server.MapPath("GroupedPresentations") + "\\" + "Session_" + (session + 1).ToString()+ "\\" + oldpath);
                            }
                        }
                        else break;
                        cnt++;
                    }

                    table.Attributes.Add("style", "border:1px solid black; background:#87cefa;");
                    Panel1.Controls.Add(table);
                    Panel1.Controls.Add(new LiteralControl("<br/>"));
                    session++;
                }
                if (cnt < n)
                {
                    //create folder named session+1
                    if (mysession.Equals("Nurlan")) System.IO.Directory.CreateDirectory(Server.MapPath("GroupedPresentations") + "\\" + "Session_" + (session + 1).ToString());
                    for (int j = 0; j < m; j++)
                    {
                        Label labelm = new Label();
                        labelm.Text = "<b>Session:" + (session + 1).ToString() + " 15.00-18.00" + "</b>";
                        Panel1.Controls.Add(labelm);
                        Panel1.Controls.Add(new LiteralControl("<br/><br/>"));
                       

                        Label label2 = new Label();
                        label2.Text = "<b>Hall: " + (j + 1).ToString() + "_" + section + "</b>";
                        Panel1.Controls.Add(label2);
                        Panel1.Controls.Add(new LiteralControl("<br/>"));

                        Table table = new Table();
                        TableRow tRow = new TableRow();
                        tRow.Attributes.Add("style", "border:1px solid black;");
                        table.Rows.Add(tRow);
                        TableCell tc4 = new TableCell();
                        tc4.Text = "<b>Date</b>";
                        tc4.Attributes.Add("style", "border:1px solid black;");
                        tRow.Cells.Add(tc4);
                        TableCell tc5 = new TableCell();
                        tc5.Text = "<b>Time</b>";
                        tc5.Attributes.Add("style", "border:1px solid black;");
                        tRow.Cells.Add(tc5);
                        TableCell tc2 = new TableCell();
                        tc2.Text = "<b>Presenter(s)</b>";
                        tc2.Attributes.Add("style", "border:1px solid black;");
                        tRow.Cells.Add(tc2);
                        TableCell tc3 = new TableCell();
                        tc3.Text = "<b>Topic</b>";
                        tc3.Attributes.Add("style", "border:1px solid black;");
                        tRow.Cells.Add(tc3);

                        for (int k = 0; k < 9; k++)
                        {
                            if (cnt < n)
                            {
                                TableRow trow = new TableRow();
                                trow.Attributes.Add("style", "border:1px solid black;");
                                table.Rows.Add(trow);

                                TableCell tcell5 = new TableCell();
                                tcell5.Text = startdate.AddDays(i).ToString().Substring(0, 10);
                                tcell5.Attributes.Add("style", "border:1px solid black;");
                                trow.Cells.Add(tcell5);

                                TableCell tcell4 = new TableCell();
                                tcell4.Text = Time(k, 15);
                                tcell4.Attributes.Add("style", "border:1px solid black;");
                                trow.Cells.Add(tcell4);

                                TableCell tcell2 = new TableCell();
                                tcell2.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "0").Rows[cnt]["FIO"].ToString();
                                tcell2.Attributes.Add("style", "border:1px solid black;");
                                trow.Cells.Add(tcell2);

                                TableCell tcell3 = new TableCell();
                                tcell3.Text = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "0").Rows[cnt]["Title"].ToString();
                                tcell3.Attributes.Add("style", "border:1px solid black;");
                                trow.Cells.Add(tcell3);

                                if (mysession.Equals("Nurlan"))
                                {
                                    string oldpath = sta.GetDataByStatusAndTopicAreaAndIsVirtual("7", topic, "0").Rows[cnt]["PPTPath"].ToString();
                                    copyFile(Server.MapPath("Presentations") + "\\" + oldpath, Server.MapPath("GroupedPresentations") + "\\" + "Session_" + (session + 1).ToString() + "\\" + oldpath);
                                }
                            }
                            else break;
                            cnt++;
                        }
                        table.Attributes.Add("style", "border:1px solid black; background:#87cefa;");
                        Panel1.Controls.Add(table);
                        Panel1.Controls.Add(new LiteralControl("<br/>"));
                        session++;
                    }
                }       
            }
            Panel1.Controls.Add(new LiteralControl("<br/><hr/>"));
        }
        string Time(int k, int start) //k = 0..8
        {
            string str = "";
            int h1 = start + 20 * k / 60;
            int m1 = (20 * k) % 60;

            int h2 = start + 20 * (k+1) / 60;
            int m2 = (20 * (k+1)) % 60;

            str += h1.ToString();
            str += ":";
            str += minutes(m1);
            str += "-";
            str += h2.ToString();
            str += ":";
            str += minutes(m2);
            return str;
        }
        string minutes(int min)
        {
            if (min == 0) return "00";
            return min.ToString();
        }
        int upperbound(int a, int b)
        {
            if (a % b == 0 && a != 0) return a / b;
            return a / b + 1;
        }
        void copyFile(string path1, string path2) 
        {
            try
            {
                File.Copy(path1, path2);
            }
            catch (Exception ex) { }
        }
    }
}

