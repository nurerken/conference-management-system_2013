﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Conference.aspx.cs" Inherits="Conference.Conference" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Papers.aspx">Managing papers</asp:HyperLink><br/>        
    <ul class="nav nav-tabs">
        <li id="new" class="active"><a data-toggle="tab" href="#newsubs">Conference Management</a></li>
        <li id="assed"><a data-toggle="tab" href="#assignedsubs">Users Management</a></li>
        <li id="reved"><a data-toggle="tab" href="#Div1">Submissions Management</a></li>        
    </ul>


    <script type="text/javascript">
        var arr = new Array();
        var arr2 = new Array();

        function myfunc(id)
        {
            var text = document.getElementById(id).value;
                       
            if (id.substring(20, 23)=="Tit") 
            {
                var i = id.substring(27, id.length);
                arr[i]=text;
                //alert('section'+i);    
            }
            if (id.substring(20, 23)=="Key")
            {
                var i = id.substring(30, id.length);
                arr2[i] = text;
                //alert('keywords'+i);    
            }           
        }

        function finalf()
        {
            document.getElementById('ContentPlaceHolder1_HiddenField1').value = arr.join('|');
            document.getElementById('ContentPlaceHolder1_HiddenField2').value = arr2.join('|');
            //alert('ok!'); 
        }
        
    </script>

    <div class="tab-content" id="mytabcontent">
                
        <div class="tab-pane active" id="newsubs">
            <asp:HiddenField ID="HiddenField1" runat="server" /><asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:Label ID="Label1" runat="server" Text="<h3><b>Create New Conference</b></h3>"></asp:Label><br/>
               
               <table>
                <tr>
                <td>Header Image:</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:FileUpload ID="FileUpload1" runat="server" /></td>
             </tr>
               </table>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
       <ContentTemplate>

        <table>
            <tr>
                <td>Event Full Name:</td><td><asp:TextBox ID="TextBox1" runat="server" Text="Conference 2014"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Event Web Address:</td><td><asp:TextBox ID="TextBox2" runat="server" Text="www.conference.com"></asp:TextBox></td>
            </tr>
            
            <tr>   
                <td>Home Page Notice:</td><td><asp:TextBox ID="TextBox3" runat="server" 
                    Text="Write descriptoin here" TextMode="MultiLine" Height="198px" Width="452px"></asp:TextBox></td>
            </tr>
            <tr>   
                <td>Requirements for paper:</td><td><asp:TextBox ID="TextBox4" runat="server" 
                    Text="Write descriptoin here" TextMode="MultiLine" Height="198px" Width="452px"></asp:TextBox></td>
            </tr>
            <tr>   
                <td>Contact Information:</td><td><asp:TextBox ID="TextBox5" runat="server" 
                    Text="Write descriptoin here" TextMode="MultiLine" Height="198px" Width="452px"></asp:TextBox></td>
            </tr>
            <tr>   
                <td>Conference Starting Date</td><td>         
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" 
                        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
                        ShowGridLines="True" Width="220px">
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <OtherMonthDayStyle ForeColor="#CC9966" />
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                            ForeColor="#FFFFCC" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                    </asp:Calendar>                  
                </td>
            </tr>  
             <tr>   
                <td>Abstract submission deadline:</td><td>
                    <asp:Calendar ID="Calendar2" runat="server" BackColor="#FFFFCC" 
                        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
                        ShowGridLines="True" Width="220px">
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <OtherMonthDayStyle ForeColor="#CC9966" />
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                            ForeColor="#FFFFCC" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                    </asp:Calendar>
                </td>
            </tr>              
            <tr>   
                <td>Paper submission deadline:</td><td>
                    <asp:Calendar ID="Calendar3" runat="server" BackColor="#FFFFCC" 
                        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
                        ShowGridLines="True" Width="220px">
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <OtherMonthDayStyle ForeColor="#CC9966" />
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                            ForeColor="#FFFFCC" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                    </asp:Calendar>
                </td>
            </tr>  
            <tr>   
                <td>Presentation submission deadline:</td><td>
                    <asp:Calendar ID="Calendar4" runat="server" BackColor="#FFFFCC" 
                        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
                        ShowGridLines="True" Width="220px">
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <OtherMonthDayStyle ForeColor="#CC9966" />
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                            ForeColor="#FFFFCC" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                    </asp:Calendar>
                </td>
            </tr>  
        </table>     
             </ContentTemplate>     
       </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
       <ContentTemplate>
        
        <h3>Sections</h3>
        <asp:Label ID="Label5" runat="server" Text="Enter Number of Sections"></asp:Label>

       <asp:TextBox ID="TextBox6" runat="server" OnTextChanged="text_changed" AutoPostBack="true"></asp:TextBox>
              
           <asp:Panel ID="Panel1" runat="server">
           </asp:Panel>
         </ContentTemplate>     
       </asp:UpdatePanel>

        <asp:Button ID="Button1" runat="server" Text="Create" onclick="Button1_Click" OnClientClick="finalf();"/>
      
       <br/><hr/>
            <asp:Button ID="Button2" runat="server" Text="Delete Conference data" OnClick="Button2_Click" OnClientClick = "return confirm0();"/>
        </div>
    
        <div class="tab-pane" id="assignedsubs">
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>                        

                          <asp:BoundField DataField="FIO" HeaderText="Name" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>                                                   
                          
                          <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField="Job" HeaderText="Job" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField="topicArea" HeaderText="topicArea" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                            <asp:BoundField DataField="organization" HeaderText="Organization" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
         <asp:Button ID="Button3" runat="server" Text="Create Reviewer" OnClick="click1"/><br/><hr/>
          
          <table>
            <tr>
                <td>Login:</td><td><asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
            </tr>          
          </table>
          <asp:Button ID="Button6" runat="server" Text="Reset Password" OnClick="reset"/>
         
    </div>
        
        <div class="tab-pane" id="Div1">
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                        <asp:TemplateField HeaderText = "Select" >
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                 </ItemTemplate>
                            </asp:TemplateField>

                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="FIO" HeaderText="Author(s)" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                          <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <br/>
         <asp:Button ID="Button4" runat="server" Text="Delete Selected Submissions" OnClick="click2" />
         <asp:Button ID="Button5" runat="server" Text="Delete all submissions" OnClick="click3"/>
    </div>
    </div>

     <script type="text/javascript">
        function confirm0() {
            var result = confirm('Are you sure to delete all submissions, users and conference data?');
            if (result)
                return true;
            else
                return false;
        }

        </script>
</asp:Content>
