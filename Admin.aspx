﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Conference.Admin" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
         <h3>
                <a href="editProfile.aspx">Edit Profile</a>
        </h3>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="conference.aspx">Manage Conference</asp:HyperLink><br/>
        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="Papers.aspx">Managing papers</asp:HyperLink><br/>        
        <br/><br/>
    <ul class="nav nav-tabs">
      <li id = "new" class="active"><a href="#newsubs" data-toggle="tab" >New abstracts</a></li>
      <li id = "assed"><a href="#assignedsubs" data-toggle="tab">Assigned abstracts</a></li>
      <li id = "reved"><a href="#reviewed" data-toggle="tab">Reviewed abstracts</a></li>
      <li id = "apped"><a href="#approved" data-toggle="tab">Approved abstracts</a></li>           
      <li id = "Li1"><a href="#withoutreport" data-toggle="tab">Participants without report</a></li>      
    </ul>

   <div class="tab-content" id="mytabcontent">
    <div class="tab-pane active" id="newsubs">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                          <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
        
        <asp:Button ID="Button1" runat="server" Text="Assign Reviewers" 
            onclick="Button1_Click" />
    </div>
    <div class="tab-pane" id="assignedsubs">
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                          <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="reviewed">
        <table border="1">
            <tr>
            <td style='Color:green'>5</td><td style='Color:green'>excellent</td><td> Accept (I 
                vote to accept this paper.)</td>
            </tr>
            <tr>
            <td style='Color:green'>4</td><td style='Color:green'>very good</td><td>Weak Accept 
                (I can support a vote to accept, but I won’t argue for it.)</td>
            </tr>
            <tr>
            <td style='Color:green'>3</td><td style='Color:green'>good</td><td>Neutral (I’m not 
                impressed but I don’t feel strongly for or against it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>2</td><td style='Color:red'>fair</td><td>Weak Reject (I don’t 
                like it, but I won’t vote to completely reject it.)</td>
            </tr>
            <tr>
            <td style='Color:red'>1</td><td style='Color:red'>poor</td><td>Total Reject (I 
                vote to reject this paper.)</td>
            </tr>
        </table>
        <br/>
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView3" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                        <asp:BoundField DataField="rev1_mark_abs" HeaderText="Mark 1" ItemStyle-Width="100px"><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField="rev2_mark_abs" HeaderText="Mark 2" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField= "avg_mark_abs" HeaderText="Average Mark" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                         
                          <asp:TemplateField HeaderText="Average Mark(Equivalent)">
                            <ItemTemplate>
                            <%# returnmark(Eval("avg_mark_abs"))%>
                            </ItemTemplate>
                         </asp:TemplateField>

                          <asp:TemplateField HeaderText = "Approve" >
                                <ItemTemplate>
                                    <asp:Button ID="Button2" runat="server" Text="Approve" onclick="approve_btn_click" CommandArgument=<%# Eval("sub_id") %>/>
                                    <asp:Button ID="Button3" runat="server" Text="Cancel" onclick="cancel_btn_click" CommandArgument=<%# Eval("sub_id") %>/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="approved">
       <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView4" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section"> 
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>

                         <asp:TemplateField HeaderText="Average Mark">
                            <ItemTemplate>
                            <%# returnmark(Eval("avg_mark_abs"))%>
                            </ItemTemplate>
                         </asp:TemplateField>

                         <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="withoutreport">
       <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView5" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:BoundField DataField="FIO" HeaderText="Author(s) Name"> 
                          </asp:BoundField>
                                                   
                          <asp:BoundField DataField="organization" HeaderText="Section"> 
                          </asp:BoundField>

                          <asp:BoundField DataField="Job" HeaderText="Job"> 
                          </asp:BoundField>

                          <asp:BoundField DataField="Country" HeaderText="Country"> 
                          </asp:BoundField> 

                         <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>   
          
    </div>    
</asp:Content>
