﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace Conference
{
    public partial class editProfile : System.Web.UI.Page
    {
        string userID = "";
        usersTableAdapter uta = new usersTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();

                if (!IsPostBack)
                {
                    TextBox1.Text = uta.GetDataByUserID(userID).Rows[0]["FIO"].ToString();
                    TextBox3.Text = uta.GetDataByUserID(userID).Rows[0]["Organization"].ToString();
                    TextBox4.Text = uta.GetDataByUserID(userID).Rows[0]["Country"].ToString();
                    TextBox5.Text = uta.GetDataByUserID(userID).Rows[0]["Email"].ToString();
                    TextBox6.Text = uta.GetDataByUserID(userID).Rows[0]["Accompanying"].ToString();
                    TextBox2.Text = uta.GetDataByUserID(userID).Rows[0]["Job"].ToString();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            uta.UpdateQuery(TextBox1.Text, TextBox5.Text, TextBox3.Text, TextBox2.Text, TextBox4.Text, TextBox6.Text, userID);
            Response.Redirect("MyProfile.aspx");
        }

        protected void button11_Click(object sender, EventArgs e)
        {
            if (filter(tb22.Text).Equals(filter(tb33.Text)))
            {
                int rows = 0;
                rows = uta.GetDataForAuth(userID, filter(tb11.Text)).Rows.Count;
                if (rows == 1)
                {
                    try
                    {
                        uta.UpdatePassword(filter(tb22.Text), userID);
                        FormsAuthentication.SignOut();
                        Session.Clear();
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Password was changed');");
                        Response.Write("document.location.href='login.aspx';");
                        Response.Write("</script>");
                    }
                    catch (Exception ex) { Response.Write(ex.Message); }
                }
                else
                {
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Wrong old password');");
                    Response.Write("</script>");
                }
            }
        }
        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }
    }
}