﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.IO;

namespace Conference
{
    public partial class Confirm2 : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string userID = "";
        string userType = "";
        string sub_id = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated && Session["sub_id"] != null)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                sub_id = Session["sub_id"].ToString();

                if (userType.Equals("2"))
                {
                    if (!IsPostBack)
                    {
                        //Label1.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Title"].ToString();
                        Label2.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["SubmissionTopicArea"].ToString();
                        Label3.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["submissionKeyWords"].ToString();
                        Label4.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Sub_date"].ToString().Substring(0, 10);
                        Label5.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                        Label6.Text = ReturnStatus(sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["status"].ToString());
                        Label7.Text = uta.GetDataByUserID(sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_id"].ToString()).Rows[0]["FIO"].ToString();
                        Label8.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_mark_pp"].ToString();
                        Label9.Text = uta.GetDataByUserID(sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_id"].ToString()).Rows[0]["FIO"].ToString();
                        Label10.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_mark_pp"].ToString();
                        Label12.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["avg_mark_pp"].ToString() + "-" + returnmark(sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["avg_mark_pp"].ToString());
                        TextBox1.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Comments"].ToString();
                        Label14.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                        Label15.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Organization"].ToString();
                        Label16.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Job"].ToString();
                        Label17.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Country"].ToString();
                        TextBox1.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Comments"].ToString();
                        //iframe settings..
                        
                        string filepath = Server.MapPath("Papers") + "\\" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PaperPath"].ToString();
                        FileInfo TheFile = new FileInfo(filepath);

                        if (TheFile.Exists)
                        {
                            Panel1.Controls.Add(new LiteralControl("<iframe id ='myframe' src='" + filepath + "' width='800px' height='900px'  runat='server'></iframe>"));
                        }  
                    }
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }
        string returnmark(string mark)
        {
            double x = Convert.ToDouble(mark);
            if (x >= 4.5 && x <= 5.0) return "<b style='Color:green'>excellent</b> Accept (I vote to accept this paper.)";
            if (x >= 4.0 && x < 4.5) return "<b style='Color:green'>very good</b> Weak Accept (I can support a vote to accept, but I won’t argue for it.)";
            if (x >= 3.0 && x < 4.0) return "<b style='Color:green'>good</b> Neutral (I’m not impressed but I don’t feel strongly for or against it.)";
            if (x >= 2.0 && x < 3.0) return "<b style='Color:red'>fair</b> Weak Reject (I don’t like it, but I won’t vote to completely reject it.)";
            if (x >= 1.0 && x < 2.0) return "<b style='Color:red'>poor</b>Total Reject (I vote to reject this paper.)";
            return "";
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'>Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
            }
            return "";
        }        
        protected void Button1_Click1(object sender, EventArgs e)
        {
            try
            {
                sta.UpdateStatus("6", Int32.Parse(sub_id));
                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor1(email, FIO, topic);
                string newcomments = TextBox1.Text;
                sta.UpdateComments(newcomments, Int32.Parse(sub_id));

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Approved!');");
                Response.Write("document.location.href='Papers.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message);}
        }
        protected void Button2_Click1(object sender, EventArgs e)
        {
            try
            {
                sta.UpdateStatus("-2", Int32.Parse(sub_id));
                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor2(email, FIO, topic);
                string newcomments = TextBox1.Text;
                sta.UpdateComments(newcomments, Int32.Parse(sub_id));

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Rejected!');");
                Response.Write("document.location.href='Papers.aspx';");
                Response.Write("</script>");
            }catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        void sendEmailToAuthor1(string toEmail, string toName, string topic)//approved paper
        {
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            string url = cta.GetData().Rows[0]["url"].ToString();

            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Alert[3].ParamName = "@url@"; Alert[3].ParamValue = url;
            Message.SendAlert("ToAuthorApprovePaper", toEmail, "Decision on Paper", Alert).ToString();
        }
        void sendEmailToAuthor2(string toEmail, string toName, string topic)//rejected paper
        {
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[3];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Message.SendAlert("ToAuthorRejectAbstract", toEmail, "Decision on Paper", Alert).ToString();
        }

    }
}