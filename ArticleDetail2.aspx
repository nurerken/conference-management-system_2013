﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ArticleDetail2.aspx.cs" Inherits="Conference.ArticleDetail2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Article info</h2>

    <table style="width: 50%;" border="1">        
        <tr>
            <td class="input-large" style="width: 207px"><b>Section:</b></td>
            <td>
                <asp:Label ID="Label1" runat="server" 
                    Text="Research on iterational method to solve SLAE"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Topic:</b></td>
            <td><asp:Label ID="Label2" runat="server" 
                    Text="Mechanics, mathematical modeling and information technologies in the oil and gas sector"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Keywords:</b></td>
            <td><asp:Label ID="Label3" runat="server" Text="iterative methods,modeling"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Submission date:</b></td>
            <td><asp:Label ID="Label4" runat="server" Text="01.02.2013"></asp:Label></td>
        </tr>
        <tr>
            <td class="input-large" style="width: 207px"><b>Status:</b></td>
            <td><asp:Label ID="Label6" runat="server" Text="Under evaluating process"></asp:Label></td>
        </tr>
       
    </table>

    <br/>
    <p>Abstract</p>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    <br/> 

    <p>Paper</p>
    <asp:Panel ID="Panel2" runat="server">
    </asp:Panel>
    <br/> 
  </asp:Content>
