﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="replace.aspx.cs" Inherits="Conference.replace" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <br />
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Upload"  OnClientClick="return checkFileExtension();"/>

     <script type="text/javascript">

         function checkFileExtension() {
             var filePath = document.getElementById("ContentPlaceHolder1_FileUpload1").value;

             if (filePath.indexOf('.') == -1)
                 return false;


             var validExtensions = new Array();
             var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

             validExtensions[0] = 'pdf';

             for (var i = 0; i < validExtensions.length; i++) {
                 if (ext == validExtensions[i])
                     return true;
             }

             alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
             filePath.value = "";
             return false;
         }
    </script>
</asp:Content>
