﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Web.UI.HtmlControls;

namespace Conference
{
    public partial class Master : System.Web.UI.MasterPage
    {
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        public string url;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cta.GetData().Rows.Count != 0)
            {
                url = "images" + "/" + cta.GetData().Rows[0]["HeaderImage"].ToString();
                if (!url.Equals(""))
                {
                    mainimg.Style.Add("background-image", url);
                    mainimg.Style.Add("background-repeat", "no-repeat");
                    mainimg.Style.Add("width", "728px");
                    mainimg.Style.Add("height", "210px");
                    mainimg.Style.Add("text-align", "right");

                    h1.InnerText = cta.GetData().Rows[0]["C_name"].ToString().Replace("\n", "<br/>");
                }
            }
        }
    }
}