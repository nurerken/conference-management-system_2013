﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;

namespace Conference
{
    public partial class CreateUser : System.Web.UI.Page
    {
        static string author_id = "";
        static string userType = "";
        usersTableAdapter uta = new usersTableAdapter();
        sectionsTableAdapter secta = new sectionsTableAdapter();
        countriesTableAdapter cta = new countriesTableAdapter();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                author_id = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(author_id).Rows[0]["type"].ToString();
                
                if (userType.Equals("2"))
                {
                    if (!IsPostBack)
                    {
                        DropDownList1.DataSource = secta.GetData();
                        DropDownList1.DataTextField = "Title";
                        DropDownList1.DataValueField = "s_id";
                        DropDownList1.DataBind();

                        DropDownList2.DataSource = cta.GetData();
                        DropDownList2.DataTextField = "Name";
                        DropDownList2.DataValueField = "c_id";
                        DropDownList2.DataBind();     
                    }
                }
               else
               {
                Response.Redirect("default.aspx");
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string keywords = "";
            IEnumerable<string> CheckedItems = ckeckboxlist1.Items.Cast<ListItem>()
                                   .Where(i => i.Selected)
                                   .Select(i => i.Value);
            foreach (string i in CheckedItems)
                keywords += i + ",";

            string login = TextBox1.Text;
            int n = uta.GetData().Rows.Count;
            bool b = false;

            for (int i = 0; i < n; i++)
            {
                if (uta.GetData().Rows[i]["user_id"].ToString().Equals(login))
                {
                    b = true;
                    break;
                }
            }
            //System.Diagnostics.Debug.Write("ok0");
            if (!b)
            {
                //System.Diagnostics.Debug.Write("ok1");
                if (TextBox1.Text != "" && TextBox2.Text != "" && TextBox3.Text != "" && TextBox4.Text != "" && TextBox5.Text != "" && DropDownList2.SelectedValue != "" && CheckedItems.Count() != 0 && (!DropDownList1.SelectedValue.Equals("") || DropDownList1.SelectedValue != null))
                {
                    //System.Diagnostics.Debug.Write("ok2");
                    uta.InsertQuery(TextBox1.Text, "123", TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, DropDownList2.SelectedValue, "1", DropDownList1.SelectedValue, keywords, "1", "1", "0", "", "");
                    
                    Response.Write("<script type='text/javascript'>");
                    //Response.Write("alert('User with username '" + TextBox1.Text + " and Password 123 created.);");
                    Response.Write("alert('User created!');");
                    Response.Write("document.location.href='Conference.aspx';");
                    Response.Write("</script>");
                }
                else
                {
                    if (CheckedItems.Count() == 0)
                    {
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Please, select keywords");
                        Response.Write("</script>");
                    }
                    else 
                    {
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Please, fill all fields");
                        Response.Write("</script>");
                    }
                }
            }
        }

        protected void changed(object sender, EventArgs e)
        {
            ckeckboxlist1.Items.Clear();
            string selectedValue = DropDownList1.SelectedValue;
            string[] keywords = secta.GetDataBy1(Convert.ToInt32(selectedValue)).Rows[0]["Keywords"].ToString().Split(',');

            for (int i = 0; i < keywords.Length; i++)
            {
                ckeckboxlist1.Items.Add(keywords[i]);
            }
        }

    }
}