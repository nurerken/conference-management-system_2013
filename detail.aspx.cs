﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Text;

namespace Conference
{
    public partial class detail : System.Web.UI.Page
    {
        string author_id, userType;
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string sub_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                author_id = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(author_id).Rows[0]["type"].ToString();
                
                if (userType.Equals("0"))
                {
                  if(Session["sub_id"]!=null){
                    sub_id = Session["sub_id"].ToString();
                    if (!IsPostBack)
                    {
                       string status = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["status"].ToString();
                        DateTime paperdeadline = Convert.ToDateTime(cta.GetData().Rows[0]["paperSubDeadline"].ToString());
                        DateTime PPTdeadline = Convert.ToDateTime(cta.GetData().Rows[0]["PresSubDeadline"].ToString());
                        DateTime startDate = Convert.ToDateTime(cta.GetData().Rows[0]["startdate"].ToString());
                        
                        Label1.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Title"].ToString();
                        Label2.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["SubmissionTopicArea"].ToString();
                        Label8.Text = ReturnStatus(status);
                        
                        HyperLink1.NavigateUrl = "Abstracts/" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["AbstractPath"].ToString();                        
                        HyperLink2.NavigateUrl = "Papers/" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PaperPath"].ToString();                        
                        HyperLink3.NavigateUrl = "Presentations/" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PPTPath"].ToString();

                        string PaperPath = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PaperPath"].ToString();
                        string PPTPath = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PPTPath"].ToString();

                        if ((PaperPath.Equals("")) && status.Equals("3") && (System.DateTime.Today <= paperdeadline))
                        {
                            Panel1.Visible = false;
                            Panel2.Visible = true;
                        }
                        else 
                        {
                            Panel2.Visible = false;
                            Panel1.Visible = true;

                            if (!PaperPath.Equals("")) HyperLink2.Visible = true;
                            else HyperLink2.Visible = false;

                            if ((!PaperPath.Equals("")) && System.DateTime.Today < startDate) Button4.Visible = true;
                            else Button4.Visible = false;
                        }

                        if ((PPTPath.Equals("")) && status.Equals("6") && (System.DateTime.Today <= PPTdeadline))
                        {
                            Panel3.Visible = false;
                            Panel4.Visible = true;
                        }
                        else 
                        {
                            Panel4.Visible = false;
                            Panel3.Visible = true;

                            if (!PPTPath.Equals("")) HyperLink3.Visible = true;
                            else HyperLink3.Visible = false;

                            if ((!PPTPath.Equals("")) && System.DateTime.Today <= PPTdeadline) Button5.Visible = true;
                            else Button5.Visible = false;
                        }                      

                        //Replace abstract button
                        if (System.DateTime.Today > paperdeadline)
                        {
                            Button3.Visible = false;
                        }

                        FileUpload1.Attributes.Add("onchange", "return checkFileExtension();");
                        FileUpload2.Attributes.Add("onchange", "return checkFileExtension2();");
                    }
                  }
                  else Response.Redirect("myprofile.aspx");
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'>Non Assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract Assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract Reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract Approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           try
            {
                if (FileUpload1.HasFile)
                {
                    string exttension = System.IO.Path.GetExtension(FileUpload1.FileName);

                    if (exttension.Equals(".pdf"))
                    {
                        string path = author_id + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(FileUpload1.FileName));
                        FileUpload1.SaveAs(Server.MapPath("Papers") + "\\" + path);
                        sta.UpdatePaperPath(path, Int32.Parse(sub_id));
                        sta.UpdateStatus("4",Int32.Parse(sub_id));

                        string rev1ID = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_id"].ToString();
                        string rev2ID = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_id"].ToString();
                        string rev1Email = uta.GetDataByUserID(rev1ID).Rows[0]["email"].ToString();
                        string rev2Email = uta.GetDataByUserID(rev2ID).Rows[0]["email"].ToString();
                        string rev1FIO = uta.GetDataByUserID(rev1ID).Rows[0]["FIO"].ToString();
                        string rev2FIO = uta.GetDataByUserID(rev2ID).Rows[0]["FIO"].ToString();
                        string section = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["submissiontopicarea"].ToString();
                        string title = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                        string Sub_date = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Sub_date"].ToString();
                        sendEmailToReviewer(rev1Email, rev1FIO, section,title,Sub_date);
                        sendEmailToReviewer(rev2Email, rev2FIO, section, title, Sub_date);

                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Uploaded');");
                        Response.Write("document.location.href='detail.aspx';");
                        Response.Write("</script>");
                    }
                }               
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload2.HasFile)
                {
                    string exttension = System.IO.Path.GetExtension(FileUpload2.FileName);

                    if (exttension.Equals(".ppt") || exttension.Equals(".pptx") || exttension.Equals(".pps"))
                    {
                        string path = author_id + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(FileUpload2.FileName));
                        FileUpload2.SaveAs(Server.MapPath("Presentations") + "\\" + path);
                        sta.UpdatePPTPath(path, Int32.Parse(sub_id));
                        sta.UpdateStatus("7", Int32.Parse(sub_id));
                        
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Uploaded');");
                        Response.Write("document.location.href='detail.aspx';");
                        Response.Write("</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'A' && str[i] <= 'z'))
                    sb.Append(str[i]);
            }

            return sb.ToString();
        }

        void sendEmailToReviewer(string toEmail, string toName, string section, string title, string s_date)//new paper uploaded
        {
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            string url = cta.GetData().Rows[0]["url"].ToString();

            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[6];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@ConfTitle@"; Alert[1].ParamValue = conftitle;
            Alert[2].ParamName = "@title@"; Alert[2].ParamValue = title;
            Alert[3].ParamName = "@section@"; Alert[3].ParamValue = section;
            Alert[4].ParamName = "@s_date@"; Alert[4].ParamValue = s_date;
            Alert[5].ParamName = "@url@"; Alert[5].ParamValue = url;
            Message.SendAlert("ToReviewer", toEmail, "New Paper", Alert).ToString();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Session["type"] = 0;
            Session["sub_id"] = sub_id;
            Response.Redirect("replace.aspx");
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            Session["type"] = 1;
            Session["sub_id"] = sub_id;
            Response.Redirect("replace.aspx");
        }
        protected void Button5_Click(object sender, EventArgs e)
        {
            Session["type"] = 2;
            Session["sub_id"] = sub_id;
            Response.Redirect("replace2.aspx");
        }
    }
}