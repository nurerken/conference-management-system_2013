﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="editProfile.aspx.cs" Inherits="Conference.editProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="1">
        <tr>
            <td>
                Name:</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Job:
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Organization:</td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Country:</td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Email:</td>
            <td>
                <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Accompanying:</td>
            <td>
                <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Button ID="Button1" runat="server" Text="Edit" onclick="Button1_Click" />
    <br/><hr/>
    <asp:Panel ID="Panel1" runat="server" >    
      <table>  
        <tr>
            <td>
                <asp:Label ID="lable22" runat="server" Text="Current Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox id="tb11" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                    ControlToValidate="tb11" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>            
            <td>
                <asp:label ID="lable33" runat="server" text="New Password:"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb22" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                    ControlToValidate="tb22" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:label ID="lable44" runat="server" text="Confirm New Password"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb33" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" 
                    ControlToValidate="tb33" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="tb22" ControlToValidate="tb33" 
                    ErrorMessage="Passwords do not match" ValidationGroup="v2"></asp:CompareValidator>
            </td>
        </tr>
      </table>
    <asp:button id="button11" runat="server" text="Change Password" 
            onclick="button11_Click" ValidationGroup="v2" />
    </asp:Panel>
</asp:Content>
