﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;

namespace Conference
{
    public partial class registration : System.Web.UI.Page
    {
        countriesTableAdapter cta = new countriesTableAdapter();
        usersTableAdapter uta = new usersTableAdapter();
        conferencedataTableAdapter cdta = new conferencedataTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack){
                DropDownList1.DataSource = cta.GetData();
                DropDownList1.DataValueField = "c_id";
                DropDownList1.DataTextField = "Name";
                DropDownList1.DataBind();
            }
        }

        protected void register(object sender, EventArgs e)
        {
            string login = TextBox_2.Text;
            int n = uta.GetData().Rows.Count;
            bool b = false;

            for (int i = 0; i < n; i++)
            {
                if (uta.GetData().Rows[i]["user_id"].ToString().Equals(login))
                {
                    b = true;
                    break;
                }
            }

            if (!b)
            {
                if(TextBox_2.Text!="" && TextBox_1.Text!="" &&TextBox1.Text!="" && TextBox3.Text!="" &&TextBox4.Text!="" && TextBox5.Text!="") {
                    uta.InsertQuery(TextBox_2.Text, TextBox_1.Text, TextBox1.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, DropDownList1.SelectedItem.ToString(), "0", "", "", "", "1", "0", TextBox2.Text, CheckBox1.Checked.ToString());
                    sendEmailToAuthor(TextBox3.Text, TextBox1.Text);
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Registration complited');");
                    Response.Write("document.location.href='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Login already exists. Please, choose another one');");         
                Response.Write("</script>");
            }              
        }

        void sendEmailToAuthor(string toEmail, string toName)
        {
            string c_name = cdta.GetData().Rows[0]["C_name"].ToString();
            string url = cdta.GetData().Rows[0]["URL"].ToString();
            string startdate = cdta.GetData().Rows[0]["startdate"].ToString().Substring(0, 10);
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@ConfTitle@"; Alert[1].ParamValue = c_name;
            Alert[2].ParamName = "@date@"; Alert[2].ParamValue = startdate;
            Alert[3].ParamName = "@URL@"; Alert[3].ParamValue = url;
            Message.SendAlert("Registration", toEmail, "Registration", Alert).ToString();
        }
    }
}