﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="registration.aspx.cs" Inherits="Conference.registration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <table border="0" cellpadding="4" cellspacing="0" class="whitebgBorder" 
        width="100%">
        <tr>
            <td bgcolor="#5E8444" class="reqFormsubHead" colspan="4" nowrap>
                <span class="fontBlackBold FontColorStyle1"><strong><span class="style3">REGISTRATION</span></strong></span>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Login:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_2" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_1" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px" TextMode="Password"></asp:TextBox>
                <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Re-enter password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox0" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px" TextMode="Password"></asp:TextBox>
                <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <span id="ContentPlaceHolder1_Label77" style="font-weight:bold;">Author(s) Name:</span></td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
     
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Email:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label12" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Organization:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="Job:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox5" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label20" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Country:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
                <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
         <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label57" runat="server" Font-Bold="True" Text="Accompanying:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>

        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label77" runat="server" Font-Bold="True" Text="With Report:"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="CheckBox1" runat="server" />
                <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        
        <tr>
        <td></td>
            <td>
                  <asp:Button ID="Button1" runat="server" Font-Bold="True" Font-Size="Medium" 
                  Font-Strikeout="False" Font-Underline="False" onclick="register" 
                  Text="Register" OnClientClick="return check();"/>
            </td>
        </tr>
    </table> 

      <script type="text/javascript">

          function validateEmail(text) {
              var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
              if (reg.test(text) == false) {
                  return false;
              }
              return true;
          }

          function check() {
              var t1 = document.getElementById("ContentPlaceHolder1_TextBox_2").value;

              var t2 = document.getElementById("ContentPlaceHolder1_TextBox_1").value;

              var t3 = document.getElementById("ContentPlaceHolder1_TextBox0").value;
              var t4 = document.getElementById("ContentPlaceHolder1_TextBox1").value;
              var t5 = document.getElementById("ContentPlaceHolder1_TextBox3").value;
              var t6 = document.getElementById("ContentPlaceHolder1_TextBox4").value;
              var t7 = document.getElementById("ContentPlaceHolder1_TextBox5").value;

              if (t1.toString() == "" || t2.toString() == "" || t3.toString() == "" || t4.toString() == "" || t5.toString() == "" || t6.toString() == "" || t7.toString() == "") {
                  alert("please, fill all required fields");
                  return false;
              }
              if (!validateEmail(t5.toString())) {
                  alert("Please, enter correct email address");
                  return false;
              }
              if (t2 != t3) {
                  alert("Passwords do not match");
                  return false;
              }

              return true;
          }
    </script>

</asp:Content>
