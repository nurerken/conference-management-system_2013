﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Conference.DataSet1TableAdapters;
using System.Text;


namespace Conference
{
    public partial class Submission2 : System.Web.UI.Page
    {
        static string author_id = "";
        static string userType = "";
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        sectionsTableAdapter secta = new sectionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string selectedValue = "";
            
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                author_id = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(author_id).Rows[0]["type"].ToString();
                int subs = Int32.Parse(uta.GetDataByUserID(author_id).Rows[0]["submissions"].ToString());
                
                DateTime deadline = Convert.ToDateTime(cta.GetData().Rows[0]["ArticleSubDeadline"].ToString());
                if (System.DateTime.Today > deadline)
                {
                    Response.Redirect("myprofile.aspx");
                }
                else
                {

                    if ((userType.Equals("0") || userType.Equals("1")) && subs == 1)
                    {
                        if (!IsPostBack)
                        {
                            ckeckboxlist1.Items.Clear();
                            selectedValue = sta.GetSubmissionsForAuthor(author_id).Rows[0]["SubmissionTopicArea"].ToString();
                            Label10.Text = selectedValue;
                            string[] keywords = secta.GetDataBy2((selectedValue)).Rows[0]["Keywords"].ToString().Split(',');

                            for (int i = 0; i < keywords.Length; i++)
                            {
                                ckeckboxlist1.Items.Add(keywords[i]);
                            }

                            FileUpload1.Attributes.Add("onchange", "return checkFileExtension();");
                        }
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void ckeckboxlist1_SelectedIndexChanged(object sender, EventArgs e) 
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string keywords = "";
            IEnumerable<string> CheckedItems = ckeckboxlist1.Items.Cast<ListItem>()
                                   .Where(i => i.Selected)
                                   .Select(i => i.Value);
            foreach (string i in CheckedItems)
                keywords += i + ",";

            string title = TextBox1.Text,
                    topic = sta.GetSubmissionsForAuthor(User.Identity.Name.ToString()).Rows[0]["SubmissionTopicArea"].ToString(),
                    sub_date = System.DateTime.Today.ToString(),
                    author_id = User.Identity.Name.ToString(),
                    abstract_path = "",
                    rev1_id = "",
                    rev2_id = "",
                    rev1_mark = 0 + "",
                    rev2_mark = 0 + "",
                    avg_mark= 0 + "",
                    add_comments="",
                    status = 0 + "";
            
            string isvirtual = "0";
            if (CheckBox1.Checked) isvirtual = "1";

            try
            {
                if (FileUpload1.HasFile)
                {
                    string exttension = System.IO.Path.GetExtension(FileUpload1.FileName);
                
                    if (!title.Equals("") && exttension.Equals(".pdf") && CheckedItems.Count()!=0)
                    {                        
                        abstract_path = author_id + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(FileUpload1.FileName));
                        FileUpload1.SaveAs(Server.MapPath("Abstracts") + "\\" + abstract_path);
                        
                        sta.InsertNewSubmission(title, topic, keywords, Convert.ToDateTime(sub_date), author_id, abstract_path, "", "", "", "", "0", "0", "0", "0", "0", "0", "", status, isvirtual);
                        
                        int oldSubmissionNumber = Int32.Parse(uta.GetDataByUserID(author_id).Rows[0]["submissions"].ToString());
                        int newSubmissionNumber = ++oldSubmissionNumber;
                        uta.UpdateSubmissionNumber(newSubmissionNumber.ToString(), author_id);
                        
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Submitted');");
                        Response.Write("document.location.href='myprofile.aspx';");
                        Response.Write("</script>");
                    }
                    else 
                    {
                        if (!exttension.Equals("pdf")) 
                        {
                            Response.Write("<script type='text/javascript'>");
                            Response.Write("alert('You are uploading wrong file");
                            Response.Write("</script>");
                        }
                        if (CheckedItems.Count() == 0)
                        {
                            Response.Write("<script type='text/javascript'>");
                            Response.Write("alert('Please, select keywords");
                            Response.Write("</script>");
                        }
                        if (title.Equals(""))
                        {
                            Response.Write("<script type='text/javascript'>");
                            Response.Write("alert('Please, fill field title");
                            Response.Write("</script>");
                        }                       
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }                    
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'A' && str[i] <= 'z'))
                    sb.Append(str[i]);
            }

            return sb.ToString();
        }
    }
}
