﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Collections;
using Conference;

namespace Conference
{
    public partial class Admin : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string userID = "",userType;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                //Response.Write(userID);
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                if (userType.Equals("2"))
                {
                    
                    Label1.Text = "Number of new abstracts: " + sta.GetDataByStatus("0").Rows.Count.ToString();
                    GridView1.DataSource = sta.GetDataByStatus("0");
                    GridView1.DataBind();

                    Label2.Text = "Number of assigned abstracts: " + sta.GetDataByStatus("1").Rows.Count.ToString();
                    GridView2.DataSource = sta.GetDataByStatus("1");
                    GridView2.DataBind();

                    Label3.Text = "Number of reviewed abstracts: " + sta.GetDataByStatus("2").Rows.Count.ToString();
                    GridView3.DataSource = sta.GetDataByStatus("2");
                    GridView3.DataBind();

                    Label4.Text = "Number of approved abstracts: " + sta.GetDataByStatus("3").Rows.Count.ToString();
                    GridView4.DataSource = sta.GetDataByStatus("3");
                    GridView4.DataBind();

                    Label5.Text = "Number of participants without report: " + uta.GetUsersWithoutReport().Rows.Count.ToString();
                    GridView5.DataSource = uta.GetUsersWithoutReport();
                    GridView5.DataBind();

                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'> Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }
        protected string ReturnDate(object val)
        {
            return val.ToString().Substring(0, 10);
        }
        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/ArticleDetail.aspx");
        }
        protected void LinkButton_Click2(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/Confirm.aspx");
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            (new Assigning()).assign();
            System.Diagnostics.Debug.WriteLine("XXXXXXXXX");
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Assigning process succesfully completed!');");
            Response.Write("document.location.href='Admin.aspx';");
            Response.Write("</script>");  
        }
        protected void approve_btn_click(Object sender, EventArgs e)
        {
            try
            {
                string sub_id = (sender as Button).CommandArgument;
                sta.UpdateStatus("3", Int32.Parse(sub_id));

                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor1(email, FIO,topic);

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Approved!');");
                Response.Write("document.location.href='Admin.aspx';");
                Response.Write("</script>");
            }
            catch(Exception ex){Console.WriteLine(ex.Message);}
            
        }
      
        protected void cancel_btn_click(Object sender, EventArgs e)
        {
            try
            {
                string sub_id = (sender as Button).CommandArgument;
                sta.UpdateStatus("-1", Int32.Parse(sub_id));
            
                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor2(email, FIO, topic);

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Rejected!');");
                Response.Write("document.location.href='Admin.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
      
        protected string returnmark(object mark)
        {
            double x = Convert.ToDouble(mark.ToString());
            if (x >= 4.5 && x <= 5.0) return "<b style='Color:green'>excellent</b>";
            if (x >= 4.0 && x < 4.5) return "<b style='Color:green'>very good</b>";
            if (x >= 3.0 && x < 4.0) return "<b style='Color:green'>good</b>";
            if (x >= 2.0 && x < 3.0) return "<b style='Color:red'>fair</b>";
            if (x >= 1.0 && x < 2.0) return "<b style='Color:red'>poor</b>";
            return "";
        }
        void sendEmailToAuthor1(string toEmail, string toName,string topic)//approved abstract
        {
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
            string url = cta.GetData().Rows[0]["url"].ToString();
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Alert[3].ParamName = "@url@"; Alert[3].ParamValue = url;
            Message.SendAlert("ToAuthorApproveAbstract", toEmail, "Decision on Abstract", Alert).ToString();
        }
        void sendEmailToAuthor2(string toEmail, string toName, string topic)//rejected abstract
        {
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[3];
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            string url = cta.GetData().Rows[0]["url"].ToString();
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Message.SendAlert("ToAuthorRejectAbstract", toEmail, "Decision on Abstract", Alert).ToString();
        }
    }
}