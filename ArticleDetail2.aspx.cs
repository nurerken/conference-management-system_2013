﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.IO;

namespace Conference
{
    public partial class ArticleDetail2 : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        string userID = "";
        string userType = "";
        string sub_id = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated && Session["sub_id"] != null)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                sub_id = Session["sub_id"].ToString();
                Label1.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Title"].ToString();
                Label2.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["SubmissionTopicArea"].ToString();
                Label3.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["submissionKeyWords"].ToString();
                Label4.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Sub_date"].ToString().Substring(0,10);
                Label6.Text = ReturnStatus(sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["status"].ToString());
                //iframe settings..

                string filepath = Server.MapPath("Abstracts") + "\\" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["AbstractPath"].ToString();
                FileInfo TheFile = new FileInfo(filepath);

                if (TheFile.Exists)
                {
                    Panel1.Controls.Add(new LiteralControl("<iframe id ='myframe' src='" + filepath + "' width='800px' height='900px'  runat='server'></iframe>"));
                }

                filepath = Server.MapPath("Papers") + "\\" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["PaperPath"].ToString();
                TheFile = new FileInfo(filepath);

                if (TheFile.Exists)
                {
                    Panel2.Controls.Add(new LiteralControl("<iframe id ='myframe' src='" + filepath + "' width='800px' height='900px'  runat='server'></iframe>"));
                }  
            }
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'>Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }
    }
}