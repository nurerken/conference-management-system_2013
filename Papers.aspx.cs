﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Ionic.Zip;


namespace Conference
{
    public partial class Papers : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        string userID = "",userType="";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                if (userType.Equals("2"))
                {
                    if (!IsPostBack)
                    {
                        Label1.Text = "Number of new papers: " + sta.GetDataByStatus("4").Rows.Count.ToString();
                        GridView1.DataSource = sta.GetDataByStatus("4");
                        GridView1.DataBind();

                        Label2.Text = "Number of reviewed papers: " + sta.GetDataByStatus("5").Rows.Count.ToString();
                        GridView2.DataSource = sta.GetDataByStatus("5");
                        GridView2.DataBind();

                        Label3.Text = "Number of approved papers: " + sta.GetDataByStatus("6").Rows.Count.ToString();
                        GridView3.DataSource = sta.GetDataByStatus("6");
                        GridView3.DataBind();

                        Label4.Text = "Number of sent presentations: " + sta.GetDataByStatus("7").Rows.Count.ToString();
                        GridView4.DataSource = sta.GetDataByStatus("7");
                        GridView4.DataBind();

                        if (cta.GetData().Rows.Count != 0)
                        {
                            if (cta.GetData().Rows[0]["ProgramCreated"].ToString().Equals("1"))
                            {
                                Button1.Visible = false;
                                Button6.Visible = true;
                            }
                            if (cta.GetData().Rows[0]["ProgramCreated"].ToString().Equals("0"))
                            {
                                Button1.Visible = true;
                                Button6.Visible = false;
                            }


                            if (cta.GetData().Rows[0]["JournalCreated"].ToString().Equals("1"))
                            {
                                HyperLink4.Visible = true;
                                HyperLink4.NavigateUrl = "Journal" + "/" + "journal.pdf";
                                Button4.Visible = false;
                            }
                            if (cta.GetData().Rows[0]["JournalCreated"].ToString().Equals("0"))
                            {
                                HyperLink4.Visible = !true;
                                Button4.Visible = !false;
                            }
                        }
                    }
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'> Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }
        protected string ReturnDate(object val)
        {
            return val.ToString().Substring(0, 10);
        }
        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/ArticleDetail.aspx");
        }
        protected void LinkButton_Click2(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/Confirm2.aspx");
        }
        protected void approve_btn_click(Object sender, EventArgs e)
        {
            try
            {
                string sub_id = (sender as Button).CommandArgument;
                sta.UpdateStatus("6", Int32.Parse(sub_id));
                //send email
                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor1(email, FIO, topic);

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Approved!');");
                Response.Write("document.location.href='Papers.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        protected void cancel_btn_click(Object sender, EventArgs e)
        {
            try
            {
                string sub_id = (sender as Button).CommandArgument;
                sta.UpdateStatus("-2", Int32.Parse(sub_id));
                string email = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["email"].ToString();
                string FIO = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["FIO"].ToString();
                string topic = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["title"].ToString();
                sendEmailToAuthor2(email, FIO, topic);

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Rejected!');");
                Response.Write("document.location.href='Papers.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        protected string returnmark(object mark)
        {
            double x = Convert.ToDouble(mark.ToString());
            if (x >= 4.5 && x <= 5.0) return "<b style='Color:green'>excellent</b>";
            if (x >= 4.0 && x < 4.5) return "<b style='Color:green'>very good</b>";
            if (x >= 3.0 && x < 4.0) return "<b style='Color:green'>good</b>";
            if (x >= 2.0 && x < 3.0) return "<b style='Color:red'>fair</b>";
            if (x >= 1.0 && x < 2.0) return "<b style='Color:red'>poor</b>";
            return "";
        }
        void sendEmailToAuthor1(string toEmail, string toName, string topic)//approved paper
        {
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            string url = cta.GetData().Rows[0]["url"].ToString();

            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Alert[3].ParamName = "@url@"; Alert[3].ParamValue = url;
            Message.SendAlert("ToAuthorApprovePaper", toEmail, "Decision on Paper", Alert).ToString();
        }
        void sendEmailToAuthor2(string toEmail, string toName, string topic)//rejected paper
        {
            string conftitle = cta.GetData().Rows[0]["c_name"].ToString();
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[3];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@title@"; Alert[1].ParamValue = topic;
            Alert[2].ParamName = "@ConfTitle@"; Alert[2].ParamValue = conftitle;
            Message.SendAlert("ToAuthorRejectAbstract", toEmail, "Decision on Paper", Alert).ToString();
        }
        protected void click1(Object sender, EventArgs e)
        {
            //set schedule is ready true in DB...
            cta.UpdateProgramCreated1();
            Session["mysession"] = "Nurlan";
            Response.Redirect("Schedule.aspx");
        }
        protected void journal(Object sender, EventArgs e)
        {
            int n = sta.GetReadySubmissions().Rows.Count;
            string[] filePaths = new string[n];
            string[] authors = new string[n];
            string[] organizations = new string[n];
            string[] countries = new string[n];
            string[] emails = new string[n];
            string[] topics = new string[n];


            for (int i = 0; i < n; i++) {
                filePaths[i] = sta.GetReadySubmissions().Rows[i]["PaperPath"].ToString();
                authors[i] = sta.GetReadySubmissions().Rows[i]["FIO"].ToString();
                organizations[i] = sta.GetReadySubmissions().Rows[i]["organization"].ToString();
                countries[i] = sta.GetReadySubmissions().Rows[i]["country"].ToString();
                emails[i] = sta.GetReadySubmissions().Rows[i]["email"].ToString();
                topics[i] = sta.GetReadySubmissions().Rows[i]["Title"].ToString();
            }

            generateJournal(filePaths,authors,organizations,countries,emails);
            cta.UpdateJournalCreatedTo1();
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Journal created!');");
            Response.Write("document.location.href='papers.aspx';");
            Response.Write("</script>");
        }
        protected void generateJournal(string[] fileNames, string[] authors, string[] organizations, string[] countries, string[] emails)
        {
            //string outputFileName = "out.pdf";
            //Step 1: Create a Docuement-Object
            Document document = new Document();
            try
            {
                //Step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Server.MapPath("Journal") + "\\Journal.pdf", FileMode.Create));

                //Step 3: Open the document
                document.Open();

                PdfContentByte cb = writer.DirectContent;

                //The current file path

                for (int i = 0; i < fileNames.Length; i++)
                {
                    string filename = Server.MapPath("Papers") + "\\" + fileNames[i];

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                    {
                        document.SetPageSize(reader.GetPageSizeWithRotation(1));
                        document.NewPage();

                        //Insert to Destination on the first page
                        if (pageNumber == 1)
                        {
                            Chunk fileRef = new Chunk(" ");
                            fileRef.SetLocalDestination(filename);
                            document.Add(fileRef);

                            // write the text in the pdf content
                            Paragraph paragraph = new Paragraph();
                            BaseFont bf = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("TTF")+"\\"+"times.ttf", iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED);
                            //iTextSharp.text.Font.FontFamily.TIMES_ROMAN
                            Font AuthortextFont = new Font(bf
                                                     , 14
                                                     , iTextSharp.text.Font.BOLD
                                                     , BaseColor.BLACK
                                );
                            //System.Diagnostics.Debug.Write(authors[i]+" ");
                            Chunk textChunk = new Chunk(authors[i], AuthortextFont);
                            
                            paragraph.Add(textChunk);
                            paragraph.Alignment = 1;
                            document.Add(paragraph);
                            document.Add(new Paragraph(" "));

                            //organization, country italic
                            bf = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("TTF") + "\\" + "times.ttf", iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED);
                            Font OrgtextFont = new Font(bf
                                                     , 14
                                                     , iTextSharp.text.Font.ITALIC
                                                     , BaseColor.BLACK
                                );

                            paragraph = new Paragraph();
                            textChunk = new Chunk(organizations[i] + "," + countries[i], OrgtextFont);
                            paragraph.Add(textChunk);
                            paragraph.Alignment = 1;
                            document.Add(paragraph);
                            //email
                            bf = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("TTF") + "\\" + "times.ttf", iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED);
                            Font emailFont = new Font(bf
                                                     , 14
                                                     , iTextSharp.text.Font.ITALIC
                                                     , BaseColor.BLACK
                                );

                            paragraph = new Paragraph();
                            textChunk = new Chunk("email:" + emails[i], emailFont);
                            paragraph.Add(textChunk);
                            paragraph.Alignment = 1;
                            document.Add(paragraph);
                            document.Add(new Paragraph(" "));

                            //topic
                            /*
                            Font topicFont = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN
                                                     , 14
                                                     , iTextSharp.text.Font.BOLD
                                                     , BaseColor.BLACK
                                );

                            paragraph = new Paragraph();
                            textChunk = new Chunk(topics[i].ToUpper(), topicFont);
                            paragraph.Add(textChunk);
                            paragraph.Alignment = 1;
                            document.Add(paragraph);
                            document.Add(new Paragraph(" "));
                            */
                        }

                        PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                        int rotation = reader.GetPageRotation(pageNumber);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                document.Close();
            }
        }
        protected void click21(Object sender, EventArgs e)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    ZipEntry ze = zip.AddDirectory(Server.MapPath("GroupedPresentations"));
                    zip.Save(Server.MapPath("Zip")+"\\"+"Presentations.zip");
                }

            }
            catch (System.Exception ex1)
            {
                Response.Write("exception: " + ex1);
            }

            downloadFile(Server.MapPath("Zip") + "\\" + "Presentations.zip");
        }
        void downloadFile(string path)
        {
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(System.IO.Path.GetFileName(path)));
            Response.WriteFile(path);
            Response.End();
        }
    }
    
}
