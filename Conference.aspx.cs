﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.IO;

namespace Conference
{
    public partial class Conference : System.Web.UI.Page
    {
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        sectionsTableAdapter secta = new sectionsTableAdapter();
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        string userID = "", userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();
                if (userType.Equals("2"))
                {
                    if (!IsPostBack)
                    {
                        Label2.Text = "Number of reviewers: " + uta.GetDataByType("1").Rows.Count.ToString();
                        GridView1.DataSource = uta.GetDataByType("1");
                        GridView1.DataBind();

                        Label3.Text = "Number of submissions: " + sta.GetData().Rows.Count.ToString();
                        GridView2.DataSource = sta.GetData();
                        GridView2.DataBind();

//                        Page.Form.Attributes.Add("enctype", "multipart/form-data");
                    }
                }
                else Response.Redirect("default.aspx");
            }
            else Response.Redirect("default.aspx");
        }
        protected void click1(object sender, EventArgs e)//create user
        {
            Response.Redirect("CreateUser.aspx");
        }
        protected void click2(object sender, EventArgs e)
        {
            
        }
        protected void click3(object sender, EventArgs e)
        {
            
        }
        protected void reset(object sender, EventArgs e)
        {
            string login = TextBox7.Text;
            uta.UpdatePassword("123", login);
        }
        protected void Button1_Click(object sender, EventArgs e)//create conference
        {
            string c_name = TextBox1.Text;
            string url = TextBox2.Text;
            string headerImage = "";
            HttpFileCollection hfc = Request.Files;
            string hpn = TextBox3.Text;
            string rfp = TextBox4.Text;
            string contactinfo = TextBox5.Text;
            DateTime startdate = Calendar1.SelectedDate;
            DateTime abstractdeadine = Calendar2.SelectedDate;
            DateTime paperdeadine = Calendar3.SelectedDate;
            DateTime presdeadine = Calendar4.SelectedDate;

            //System.Diagnostics.Debug.Write("before...");
            
            if (FileUpload1.HasFile)
            { 
                string exttension = System.IO.Path.GetExtension(FileUpload1.FileName);

                //System.Diagnostics.Debug.Write("ext = "+exttension);

                if (exttension.Equals(".jpg"))
                {
                    FileUpload1.SaveAs(Server.MapPath("images") + "\\" + "mainImage" + Path.GetExtension(FileUpload1.FileName));
                    headerImage = "mainImage" + Path.GetExtension(FileUpload1.FileName);        
                
                    cta.InsertQuery(c_name, url, headerImage, hpn, rfp, contactinfo, startdate, abstractdeadine, paperdeadine, presdeadine, "0", "0");

                    string[] sections = HiddenField1.Value.ToString().Split("|".ToCharArray());
                    string[] keywords = HiddenField2.Value.ToString().Split("|".ToCharArray());

                    for (int i = 0; i < sections.Length; i++)
                    {
                        secta.InsertQuery(sections[i], keywords[i]);                       
                    }

                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Created!');");
                    Response.Write("document.location.href='conference.aspx';");
                    Response.Write("</script>");
                }
            }            
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            cta.TruncateQuery();
            secta.TruncateTable();
            //uta.TruncateQuery();
            sta.TruncateQuery();
            uta.DeleteQuery();
            
            try
            {
                System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(Server.MapPath("GroupedPresentations"));
                foreach (FileInfo file in myDirInfo.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in myDirInfo.GetDirectories())
                {
                    dir.Delete(true);
                }
                myDirInfo = new DirectoryInfo(Server.MapPath("Abstracts"));
                foreach (FileInfo file in myDirInfo.GetFiles())
                {
                    file.Delete();
                }
                myDirInfo = new DirectoryInfo(Server.MapPath("Papers"));
                foreach (FileInfo file in myDirInfo.GetFiles())
                {
                    file.Delete();
                }
                myDirInfo = new DirectoryInfo(Server.MapPath("Presentations"));
                foreach (FileInfo file in myDirInfo.GetFiles())
                {
                    file.Delete();
                }

                File.Delete(Server.MapPath("Journal")+"\\Journal.pdf");
                File.Delete(Server.MapPath("Zip") + "\\Presentations.zip");


            }
            catch (System.IO.IOException ex) { System.Diagnostics.Debug.Write(ex.Message); }

            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Deleted!');");
            Response.Write("document.location.href='Conference.aspx';");
            Response.Write("</script>");
        }
        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/ArticleDetail.aspx");
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'> Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }
        protected string ReturnDate(object val)
        {
            return val.ToString().Substring(0, 10);
        }
        protected void text_changed(Object sender, EventArgs e)
        {
            bool b = true;
            for (int i = 0; i < this.TextBox6.Text.Length; i++)
            {
                if (char.IsDigit(this.TextBox6.Text[i]) == false)
                {
                    b = false; break;
                }
            }

            if (b)
            {
                int n = Int32.Parse(TextBox6.Text);

                Label label1 = new Label();
                label1.Text = "Please, enter keywords via comma";
                Panel1.Controls.Add(label1);
                Panel1.Controls.Add(new LiteralControl("<br/>"));

                Table table = new Table();
                TableRow tRow = new TableRow();
                tRow.Attributes.Add("style", "border:1px solid black;");
                table.Rows.Add(tRow);
                TableCell tc = new TableCell();
                tc.Text = "<b>N</b>";
                tc.Attributes.Add("style", "border:1px solid black;");
                tRow.Cells.Add(tc);
                TableCell tc2 = new TableCell();
                tc2.Text = "<b>Section Title</b>";
                tc2.Attributes.Add("style", "border:1px solid black;");
                tRow.Cells.Add(tc2);
                TableCell tc3 = new TableCell();
                tc3.Text = "<b>Keywords</b>";
                tc3.Attributes.Add("style", "border:1px solid black;");
                tRow.Cells.Add(tc3);

                for (int i = 0; i < n; i++)
                {
                    TableRow trow = new TableRow();
                    trow.Attributes.Add("style", "border:1px solid black;");
                    table.Rows.Add(trow);

                    TableCell tcell = new TableCell();
                    tcell.Text = (i + 1).ToString();
                    tcell.Attributes.Add("style", "border:1px solid black;");
                    trow.Cells.Add(tcell);

                    TableCell tcell2 = new TableCell();
                    TextBox tb = new TextBox();
                    tb.ID = "TitleTB" + i.ToString();

                    tb.Attributes.Add("onChange", "myfunc('" + "ContentPlaceHolder1_TitleTB" + i.ToString() + "');");
                    tcell2.Controls.Add(tb);
                    tcell2.Attributes.Add("style", "border:1px solid black;");
                    trow.Cells.Add(tcell2);

                    TableCell tcell3 = new TableCell();
                    TextBox tb2 = new TextBox();
                    tb2.ID = "KeywordsTB" + i.ToString();
                    tb2.Attributes.Add("onChange", "myfunc('" + "ContentPlaceHolder1_KeywordsTB" + i.ToString() + "');");
                    tcell3.Controls.Add(tb2);
                    tcell3.Attributes.Add("style", "border:1px solid black;");
                    trow.Cells.Add(tcell3);
                }

                Panel1.Controls.Add(table);
                Panel1.Controls.Add(new LiteralControl("<br/>"));
            }
        }
    }
}