﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Data;

namespace Conference
{
    public partial class Reviewer : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        string userID = "";
        string userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();

                if (userType.Equals("1"))
                {
                    if (!IsPostBack)
                    {
                        DataTable table11 = new DataTable();
                        table11.Columns.Add("sub_id", typeof(int)); // Add  columns.
                        table11.Columns.Add("Title", typeof(string));
                        table11.Columns.Add("submissiontopicarea", typeof(string));
                        table11.Columns.Add("sub_date", typeof(DateTime));
                        table11.Columns.Add("rev1_id", typeof(string));
                        table11.Columns.Add("rev2_id", typeof(string));
                        table11.Columns.Add("rev1_mark_abs", typeof(string));
                        table11.Columns.Add("rev2_mark_abs", typeof(string));

                        for (int i = 0; i < sta.GetDataRevIDandStatus(userID, "1").Rows.Count; i++)
                        {
                            int id = Convert.ToInt32(sta.GetDataRevIDandStatus(userID, "1").Rows[i]["sub_id"].ToString());
                            string title = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["title"].ToString();
                            string topic = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["submissiontopicarea"].ToString();
                            DateTime date = Convert.ToDateTime(sta.GetDataRevIDandStatus(userID, "1").Rows[i]["sub_date"].ToString());
                            string rev1_id = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["rev1_id"].ToString();
                            string rev2_id = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["rev2_id"].ToString();
                            string rev1_mark_pp = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["rev1_mark_abs"].ToString();
                            string rev2_mark_pp = sta.GetDataRevIDandStatus(userID, "1").Rows[i]["rev2_mark_abs"].ToString();
                            table11.Rows.Add(id, title, topic, date, rev1_id, rev2_id, rev1_mark_pp, rev2_mark_pp);
                        }

                        DataTable table22 = new DataTable();
                        table22.Columns.Add("sub_id", typeof(int)); // Add  columns.
                        table22.Columns.Add("Title", typeof(string));
                        table22.Columns.Add("submissiontopicarea", typeof(string));
                        table22.Columns.Add("sub_date", typeof(DateTime));
                        table22.Columns.Add("rev1_id", typeof(string));
                        table22.Columns.Add("rev2_id", typeof(string));
                        table22.Columns.Add("rev1_mark_abs", typeof(string));
                        table22.Columns.Add("rev2_mark_abs", typeof(string));
                        
                        for (int i = 0; i < table11.Rows.Count; i++)
                        {
                            string rev1_id = table11.Rows[i][4].ToString();
                            string rev2_id = table11.Rows[i][5].ToString();

                            if (userID.Equals(rev1_id)) //if I am rev1_id, then get rev1_mark
                            {
                                string rev1_mark = table11.Rows[i][6].ToString();

                                if (rev1_mark.Equals("0"))
                                {
                                    table22.Rows.Add(table11.Rows[i][0], table11.Rows[i][1], table11.Rows[i][2], table11.Rows[i][3], table11.Rows[i][4], table11.Rows[i][5], table11.Rows[i][6], table11.Rows[i][7]);
                                }
                            }

                            if (userID.Equals(rev2_id)) //if I am rev2_id, then get rev2_mark
                            {
                                string rev2_mark = table11.Rows[i][7].ToString();

                                if (rev2_mark.Equals("0"))
                                {
                                    table22.Rows.Add(table11.Rows[i][0], table11.Rows[i][1], table11.Rows[i][2], table11.Rows[i][3], table11.Rows[i][4], table11.Rows[i][5], table11.Rows[i][6], table11.Rows[i][7]);
                                }
                            }
                        }

                        Label1.Text = "Number of new abstracts: " + table22.Rows.Count;
                        GridView1.DataSource = table22;
                        GridView1.DataBind();

                        Label2.Text = "Number of checked abstracts: " + sta.GetDataRevIDandStatus(userID, "2").Rows.Count.ToString();
                        GridView2.DataSource = sta.GetDataRevIDandStatus(userID, "2");
                        GridView2.DataBind();


                        DataTable table = new DataTable();
                        table.Columns.Add("sub_id", typeof(int)); // Add  columns.
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("submissiontopicarea", typeof(string));
                        table.Columns.Add("sub_date", typeof(DateTime));
                        table.Columns.Add("rev1_id", typeof(string));
                        table.Columns.Add("rev2_id", typeof(string));
                        table.Columns.Add("rev1_mark_pp", typeof(string));
                        table.Columns.Add("rev2_mark_pp", typeof(string));
                        
                        for (int i = 0; i < sta.GetDataRevIDandStatus(userID, "4").Rows.Count; i++)
                        {
                            int id = Convert.ToInt32(sta.GetDataRevIDandStatus(userID, "4").Rows[i]["sub_id"].ToString());
                            string title = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["title"].ToString();
                            string topic = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["submissiontopicarea"].ToString();
                            DateTime date = Convert.ToDateTime(sta.GetDataRevIDandStatus(userID, "4").Rows[i]["sub_date"].ToString());
                            string rev1_id = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["rev1_id"].ToString();
                            string rev2_id = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["rev2_id"].ToString();
                            string rev1_mark_pp = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["rev1_mark_pp"].ToString();
                            string rev2_mark_pp = sta.GetDataRevIDandStatus(userID, "4").Rows[i]["rev2_mark_pp"].ToString();
                            table.Rows.Add(id, title, topic, date,rev1_id,rev2_id,rev1_mark_pp,rev2_mark_pp);
                        }

                        DataTable table2 = new DataTable();
                        table2.Columns.Add("sub_id", typeof(int)); // Add  columns.
                        table2.Columns.Add("Title", typeof(string));
                        table2.Columns.Add("submissiontopicarea", typeof(string));
                        table2.Columns.Add("sub_date", typeof(DateTime));
                        table2.Columns.Add("rev1_id", typeof(string));
                        table2.Columns.Add("rev2_id", typeof(string));
                        table2.Columns.Add("rev1_mark_pp", typeof(string));
                        table2.Columns.Add("rev2_mark_pp", typeof(string));


                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            string rev1_id = table.Rows[i][4].ToString();
                            string rev2_id = table.Rows[i][5].ToString();

                            if (userID.Equals(rev1_id)) //if I am rev1_id, then get rev1_mark
                            {
                                string rev1_mark = table.Rows[i][6].ToString();
                                
                                if (rev1_mark.Equals("0")) 
                                {                                    
                                    table2.Rows.Add(table.Rows[i][0], table.Rows[i][1], table.Rows[i][2], table.Rows[i][3], table.Rows[i][4], table.Rows[i][5], table.Rows[i][6], table.Rows[i][7]);
                                }
                            }

                            if (userID.Equals(rev2_id)) //if I am rev2_id, then get rev2_mark
                            {
                                string rev2_mark = table.Rows[i][7].ToString();
                                
                                if (rev2_mark.Equals("0"))
                                {
                                    table2.Rows.Add(table.Rows[i][0], table.Rows[i][1], table.Rows[i][2], table.Rows[i][3], table.Rows[i][4], table.Rows[i][5], table.Rows[i][6], table.Rows[i][7]);
                                }
                            }
                        }

                        Label3.Text = "Number of new papers: " + table2.Rows.Count;
                        GridView3.DataSource = table2;
                        GridView3.DataBind();
                        
                        
                        Label4.Text = "Number of checked papers: " + sta.GetDataRevIDandStatus(userID, "5").Rows.Count.ToString();
                        GridView4.DataSource = sta.GetDataRevIDandStatus(userID, "5");
                        GridView4.DataBind();

                        Label6.Text = "Number of all submissions for me: " + sta.GetDataByReviewerID(userID).Rows.Count.ToString();
                        GridView6.DataSource = sta.GetDataByReviewerID(userID);
                        GridView6.DataBind();
                    }
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }       
        protected string ReturnDate(object val)
        {
            return val.ToString().Substring(0, 10);
        }
        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/edit.aspx");
        }
        protected string returnmark(object mark)
        {
            double x = Convert.ToDouble(mark.ToString());
            if (x >= 4.5 && x <= 5.0) return "<b style='Color:green'>excellent</b>";
            if (x >= 4.0 && x < 4.5) return "<b style='Color:green'>very good</b>";
            if (x >= 3.0 && x < 4.0) return "<b style='Color:green'>good</b>";
            if (x >= 2.0 && x < 3.0) return "<b style='Color:red'>fair</b>";
            if (x >= 1.0 && x < 2.0) return "<b style='Color:red'>poor</b>";
            return "";
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {
                if (val.ToString().Equals("0")) return "<a style='Color:blue'> Non assigned</a>";
                if (val.ToString().Equals("1")) return "<a style='Color:blue'>Abstract assigned</a>";
                if (val.ToString().Equals("2")) return "<a style='Color:blue'>Abstract reviewed</a>";
                if (val.ToString().Equals("3")) return "<a style='Color:green'>Abstract approved</a>";
                if (val.ToString().Equals("-1")) return "<a style='Color:red'>Abstract Rejected</a>";
                if (val.ToString().Equals("4")) return "<a style='Color:blue'>Paper submitted</a>";
                if (val.ToString().Equals("5")) return "<a style='Color:blue'>Paper reviewed</a>";
                if (val.ToString().Equals("6")) return "<a style='Color:blue'>Paper approved</a>";
                if (val.ToString().Equals("7")) return "<a style='Color:blue'>Presentation sent</a>";
                if (val.ToString().Equals("-2")) return "<a style='Color:Red'>Paper rejected</a>";
            }
            return "";
        }
        protected void LinkButton_Click2(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/articledetail2.aspx");
        }
        protected void LinkButton_Click3(Object sender, EventArgs e)
        {
            string sub_id = (sender as LinkButton).CommandArgument;
            Session["sub_id"] = sub_id;
            Response.Redirect("~/edit2.aspx");
        }
       
    }

}