﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Net;
using System.IO;

namespace Conference
{
    public partial class edit : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        string userID = "";
        string userType = "";
        string sub_id = "";
        double avgmark = 0;
        static int x1=0, x2=0, x3=0, x4=0, x5=0, x6=0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = uta.GetDataByUserID(userID).Rows[0]["type"].ToString();

                if (userType.Equals("1"))
                {
                    if (Session["sub_id"] != null)
                    {
                        sub_id = Session["sub_id"].ToString();

                        if (!IsPostBack)
                        {
                            string filepath = Server.MapPath("Abstracts") + "\\" + sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["AbstractPath"].ToString();
                            FileInfo TheFile = new FileInfo(filepath);

                            if (TheFile.Exists)
                            {
                                Panel1.Controls.Add(new LiteralControl("<iframe id ='myframe' src='" + filepath + "' width='800px' height='900px'  runat='server'></iframe>"));
                            }  

                            Label2.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["submissiontopicarea"].ToString();
                            Label3.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["submissionKeyWords"].ToString();
                            Label4.Text = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["sub_date"].ToString().Substring(0,10);

                        }
                    }
                    else Response.Redirect("reviewer.aspx");
                }
                else Response.Redirect("login.aspx");
            }
            else Response.Redirect("login.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string rev1ID = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_id"].ToString();
            string rev2ID = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_id"].ToString();
            
            if (userID.Equals(rev1ID)) 
            {
                //put mark to rev1_mark
                sta.UpdateRev1MarkAbstract(getAverageMark().ToString(), Int32.Parse(sub_id));
                
                string oldcomments = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Comments"].ToString();
                string newcomments = TextBox1.Text;
                newcomments += "\n" + oldcomments;
                sta.UpdateComments(newcomments, Int32.Parse(sub_id));

                string rev1Mark = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_mark_abs"].ToString();
                string rev2Mark = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_mark_abs"].ToString();
                //rev1Mark.Replace(',', '.');
                //rev2Mark.Replace(',', '.');

                if (!rev1Mark.Equals("0")&&(!rev2Mark.Equals("0"))) 
                {
                    sta.UpdateStatus("2", Int32.Parse(sub_id));
                    double avgmark1 = Math.Round((Double.Parse(rev1Mark)+Double.Parse(rev2Mark))/2.0,2);
                    sta.UpdateAvgMarkAbs(avgmark1.ToString(),Int32.Parse(sub_id));
                }
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Reviewed');");
                Response.Write("document.location.href='reviewer.aspx';");
                Response.Write("</script>");  
            }
            if (userID.Equals(rev2ID))
            {
                //put mark to rev2_mark
                sta.UpdateRev2MarkAbstract(getAverageMark().ToString(), Int32.Parse(sub_id));
                string rev1Mark = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev1_mark_abs"].ToString();
                string rev2Mark = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["rev2_mark_abs"].ToString();
                //rev1Mark.Replace(',', '.');
                //rev2Mark.Replace(',', '.');

                string oldcomments = sta.GetDataBySubID(Int32.Parse(sub_id)).Rows[0]["Comments"].ToString();
                string newcomments = TextBox1.Text;
                newcomments += "\n" + oldcomments;
                sta.UpdateComments(newcomments, Int32.Parse(sub_id));

                if (!rev1Mark.Equals("0") && (!rev2Mark.Equals("0")))
                {
                    sta.UpdateStatus("2", Int32.Parse(sub_id));
                    double avgmark1 = Math.Round((Double.Parse(rev1Mark) + Double.Parse(rev2Mark)) / 2.0, 2);
                    sta.UpdateAvgMarkAbs(avgmark1.ToString(), Int32.Parse(sub_id));
                }
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Reviewed');");
                Response.Write("document.location.href='reviewer.aspx';");
                Response.Write("</script>");  
            }    
        }
        protected void changed1(object sender, EventArgs e)
        {
            string id = (sender as RadioButtonList).ID.ToString();
            
            if (id.Equals("RadioButtonList1")) 
            {
                x1 = Int32.Parse(RadioButtonList1.SelectedValue.ToString());                
            }
      
            if (id.Equals("RadioButtonList3"))
            {
                x2 = Int32.Parse(RadioButtonList3.SelectedValue.ToString());                
            }
            if (id.Equals("RadioButtonList5"))
            {
                x4 = Int32.Parse(RadioButtonList5.SelectedValue.ToString());                
            }
            if (id.Equals("RadioButtonList6"))
            {
                x5 = Int32.Parse(RadioButtonList6.SelectedValue.ToString());                
            }
            if (id.Equals("RadioButtonList7"))
            {
                x6 = Int32.Parse(RadioButtonList7.SelectedValue.ToString());                
            }
            settext();
            //System.Diagnostics.Debug.WriteLine(x1+" "+x2+" "+x3+" "+x4+" "+x5);
        }
        void settext() 
        {
            string str = getAverageMark().ToString();
            Label5.Text = str;
        }
        double getAverageMark() 
        {
            return Math.Round((x1 + x2 + x4 + x5 + x6) * 1.0 / 5.0, 2);
        }        
    }
}