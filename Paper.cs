﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Conference
{
    class Paper
    {
        public string ID;
        public string[] keywords;

        public Paper(String ID, string[] keywords)
        {
            this.ID = ID;
            this.keywords = keywords;
        }
    }
}
