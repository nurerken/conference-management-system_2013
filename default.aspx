﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Conference._default" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

<style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
        .modalPopup
        {
            width: 250px;
            height: 180px;
            background-color: #076DAB;
            color: #FFFFFF;
            border-color: #000000;
            border-width: 1px;
            border-style: solid;
            text-align: center;
            cursor: move;
            font-size: medium;
        }
        .reqFormsubHead
        {
            color: #FFFFFF;
        }
    </style>
    <div>
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none">        
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Login ID="Login1" runat="server"  DisplayRememberMe="False"
                    TitleText="" UserNameLabelText="Login:" OnAuthenticate="Login1_Authenticate"
                    FailureText="Bad username or password" Width="100%" VisibleWhenLoggedIn="False">
                    <LayoutTemplate>
                        <table border="0" cellpadding="1" cellspacing="0" style="border-collapse: collapse;
                            width: 100%">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl00$Login1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$Login1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <p>
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Login" ValidationGroup="ctl00$Login1"
                                            Width="70px" />
                                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel"
                                            Width="70px" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color: Red;">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    </asp:Login>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>   

    <asp:Panel ID="Panel11" runat="server" BorderWidth="0">
        <table>
            <tr>
              <td style = "width: 400px">
        <cc1:ModalPopupExtender ID="LoginStatus1_ModalPopupExtender" runat="server" 
            BackgroundCssClass="modalBackground" PopupControlID="Panel1" 
            TargetControlID="LoginStatus1">
        </cc1:ModalPopupExtender>
              </td>
            </tr>
        </table>
        
        <script>
            function clientclick() {
                var mydiv = document.getElementById("div1");
                mydiv.style.visibility = 'visible';
                return false;
            }
        </script>

    <table>
        <tr>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
            <td style="width:100px"></td>
        </tr>
     </table>    
      
     
    <ul class="nav nav-tabs">
      <li id = "conference" class="active"><a href="#conference1" data-toggle="tab"> About Conference</a></li>
      <li id = "requirements"><a href="#requirements1" data-toggle="tab">Requirements for paper</a></li>
      <li id = "Li1"><a href="#programme" data-toggle="tab">Programme</a></li>
      <li id = "Registration"><a href="#Registratoin1" data-toggle="tab">Registration</a></li>      
      <li id = "Contact"><a href="#Contact1" data-toggle="tab">Contact</a></li>            
    </ul>
    
    <div class="tab-content" id="mytabcontent">
    <div  id="conference1" class="tab-pane active">     
    <table>
        <tr>
        <td style="background-color:#EFF0F2">
           <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
         </td>
            
      </tr>
        <tr>
            <td style="background-color:#EFF0F2">
                &nbsp;</td>
        </tr>
      </table>
    </div>

    <div class="tab-pane"  id="requirements1">
         <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
      </div>    

     <div class="tab-pane" id="programme">
         <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Schedule.aspx" Visible="false">Conference Programme</asp:HyperLink>
         <asp:Label ID="Label1" runat="server" Text="The programme is not ready yet" Visible="false"></asp:Label>
    </div>      
    <div class="tab-pane" id="Registratoin1">

        <table border="0" cellpadding="4" cellspacing="0" class="whitebgBorder" 
        width="100%">
        <tr>
            <td bgcolor="#5E8444" class="reqFormsubHead" colspan="4" nowrap>
                <span class="fontBlackBold FontColorStyle1"><strong><span class="style3">REGISTRATION</span></strong></span>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label11111" runat="server" Font-Bold="True" Text="Login:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_2" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label44" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_1" runat="server" style="margin-top: 0px" TextMode="password" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label66" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label88" runat="server" Font-Bold="True" Text="Re-enter password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox0" runat="server" style="margin-top: 0px" TextMode="password" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label99" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label77" runat="server" Font-Bold="True" Text="Author(s) Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" style="margin-top: 0px" 
                    ViewStateMode="Disabled" Width="466px"></asp:TextBox>
                <asp:Label ID="Label110" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>        
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label123" runat="server" Font-Bold="True" Text="Email:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label1234" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label4314" runat="server" Font-Bold="True" Text="Organization:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label4315" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label5419" runat="server" Font-Bold="True" Text="Job:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox5" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label420" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label5417" runat="server" Font-Bold="True" Text="Country:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
                <asp:Label ID="Label5418" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>

         <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Accompanying:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" Width="466px"></asp:TextBox>
                <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>

        <tr>
            <td align="right" class="style1">
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="With Report:"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="CheckBox1" runat="server" />
                <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label>
            </td>
        </tr>

        <tr>
        <td></td>
            <td>
                  <asp:Button ID="Button11" runat="server" Font-Bold="True" Font-Size="Medium" 
                  Font-Strikeout="False" Font-Underline="False" onclick="register" OnClientClick="return check();"
                  Text="Register" />
            </td>
        </tr>
    </table>     
    </div>  
     
    <div class="tab-pane" id="Contact1">
       <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
    </div>      
        
    </div>      
    
    <script type="text/javascript">

        function validateEmail(text) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(text) == false) {
                return false;
            }
            return true;
        }

        function check() {
            var t1 = document.getElementById("ContentPlaceHolder1_TextBox_2").value;
            
            var t2 = document.getElementById("ContentPlaceHolder1_TextBox_1").value;
            
            var t3 = document.getElementById("ContentPlaceHolder1_TextBox0").value;
            var t4 = document.getElementById("ContentPlaceHolder1_TextBox1").value;
            var t5 = document.getElementById("ContentPlaceHolder1_TextBox3").value;
            var t6 = document.getElementById("ContentPlaceHolder1_TextBox4").value;
            var t7 = document.getElementById("ContentPlaceHolder1_TextBox5").value;
                        
            if (t1.toString() == "" || t2.toString() == "" || t3.toString() == "" || t4.toString() == "" || t5.toString() == "" || t6.toString() == "" || t7.toString() == "")
            {                
                alert("please, fill all required fields");
                return false;
            }
            if (!validateEmail(t5.toString())) {
                alert("Please, enter correct email address");
                return false;
            }
            if (t2 != t3) 
            {
                alert("Passwords do not match");
                return false;
            }

            return true;
        }
    </script>

    </asp:Panel>           
</asp:Content>
