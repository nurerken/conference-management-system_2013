﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Conference.DataSet1TableAdapters;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace Conference
{
    public partial class login : System.Web.UI.Page
    {
        usersTableAdapter uta = new usersTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login11_Authenticate(object sender, AuthenticateEventArgs e)
        {
            int rows = 0;
            rows = uta.GetDataForAuth(filter(Login11.UserName), filter(Login11.Password)).Rows.Count;
            e.Authenticated = (rows == 1);

            if (!e.Authenticated)
            {
                
            }
            if (e.Authenticated)
            {
                string type = uta.GetDataByUserID(filter(Login11.UserName)).Rows[0]["type"].ToString();
                if (type.Equals("0"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login11.UserName), true);
                    Response.Redirect("myprofile.aspx");
                }
                if (type.Equals("1"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login11.UserName), true);
                    Response.Redirect("reviewer.aspx");
                }
                if (type.Equals("2"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login11.UserName), true);
                    Response.Redirect("admin.aspx");
                }
                if (type.Equals("3"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login11.UserName), true);
                    Response.Redirect("moder.aspx");
                }
            }

        }

        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }


    }
}