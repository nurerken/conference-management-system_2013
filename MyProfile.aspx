﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="Conference.MyProfile" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>
                <a href="editProfile.aspx">Edit Profile</a>
    </h3>
    
    <table border="0">
        <tr>
            <td>Name:</td><td><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td>Job: </td><td><asp:Label ID="Label4" runat="server" Text="Label" ></asp:Label></td>
        </tr>
        <tr>
            <td>Organization:</td><td><asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td>Country</td><td><asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td>
                Email:</td>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Accompanying:</td>
            <td>
                <asp:Label ID="Label2" runat="server" Text="-"></asp:Label>
            </td>
        </tr>
    </table>

    <br/>
    <h3>Submissions:</h3>
        
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                                                  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>


                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                           
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>

                          <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                            <%# ReturnStatus(Eval("status")) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Button ID="Button2" runat="server" Text="Delete" 
                                CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                OnClick="delete"/>
                            </ItemTemplate>
                        </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>   

    <asp:Label ID="Label6" runat="server" Text="You have no submissions yet"></asp:Label>
    <br/>
    <asp:Button ID="Button1" runat="server" Text="New submission" 
        onclick="Button1_Click" />

</asp:Content>
