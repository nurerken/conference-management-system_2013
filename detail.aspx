﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="detail.aspx.cs" Inherits="Conference.detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label6" runat="server" Text="Article"></asp:Label>
    <table border="1">
    <tr>
        <td><asp:Label ID="Label4" runat="server" Text="Topic: " Font-Size="Large"></asp:Label></td>
        <td><asp:Label ID="Label1" runat="server" Text="Topic" Font-Size="Large"></asp:Label></td>
    </tr>
    <tr>
        <td><asp:Label ID="Label5" runat="server" Text="Section: " Font-Size="Large"></asp:Label></td>
        <td><asp:Label ID="Label2" runat="server" Text="Section" Font-Size="Large"></asp:Label></td>
    </tr>
    <tr>
        <td><asp:Label ID="Label3" runat="server" Text="Status: " Font-Size="Large"></asp:Label></td>
        <td><asp:Label ID="Label8" runat="server" Text="Section" Font-Size="Large"></asp:Label></td>
    </tr>
    </table>

    <br/>
    <ul class="nav nav-tabs">
      <li id = "abstract" class="active"><a href="#Abstract1" data-toggle="tab">Abstract</a></li>
      <li id = "Paper"><a href="#Paper1" data-toggle="tab">Paper</a></li>
      <li id = "PPT"><a href="#PPT1" data-toggle="tab">Presentation</a></li>            
    </ul>
    
    <div class="tab-content" id="mytabcontent">
    <div  id="Abstract1" class="tab-pane active"> 
        <asp:HyperLink ID="HyperLink1" runat="server">Download</asp:HyperLink>     
        <asp:Button ID="Button3" runat="server" Text="Replace" 
            onclick="Button3_Click" />   <br/>
    </div>
    <div class="tab-pane"  id="Paper1">
        <asp:Panel ID="Panel1" runat="server">
            <asp:HyperLink ID="HyperLink2" runat="server">Download</asp:HyperLink>
            <asp:Button ID="Button4" runat="server" Text="Replace" onclick="Button4_Click" />   <br/>     
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="Label7" runat="server" Text="Upload paper"></asp:Label><br/>
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:Button ID="Button1" runat="server" Text="Upload" OnClick="Button1_Click" Visible="true"  />
        </asp:Panel>
    </div>
    <div class="tab-pane"  id="PPT1">
        <asp:Panel ID="Panel3" runat="server">
            <asp:HyperLink ID="HyperLink3" runat="server">Download Presentation</asp:HyperLink>      
            <asp:Button ID="Button5" runat="server" Text="Replace"  OnClick="Button5_Click"/> <br/>
        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server">
            <asp:Label ID="Label10" runat="server" Text="Upload Power Point presentation"></asp:Label><br/>
            <asp:FileUpload ID="FileUpload2" runat="server"/>
            <asp:Button ID="Button2" runat="server" Text="Upload" OnClick="Button2_Click" />
        </asp:Panel>
    </div>
    </div>
        <script type="text/javascript">

            function checkFileExtension() {
                var filePath = document.getElementById("ContentPlaceHolder1_FileUpload1").value;

                if (filePath.indexOf('.') == -1)
                    return false;


                var validExtensions = new Array();
                var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

                validExtensions[0] = 'pdf';

                for (var i = 0; i < validExtensions.length; i++) {
                    if (ext == validExtensions[i])
                        return true;
                }

                alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
                filePath.value = "";
                return false;
            }

            function checkFileExtension2() {
                var filePath = document.getElementById("ContentPlaceHolder1_FileUpload2").value;

                if (filePath.indexOf('.') == -1)
                    return false;


                var validExtensions = new Array();
                var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

                validExtensions[0] = 'ppt';
                validExtensions[1] = 'pptx';
                validExtensions[2] = 'pps';

                for (var i = 0; i < validExtensions.length; i++) {
                    if (ext == validExtensions[i])
                        return true;
                }

                alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
                filePath.value = "";
                return false;
            }
  
    </script>
</asp:Content>
