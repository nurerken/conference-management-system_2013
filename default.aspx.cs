﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Conference.DataSet1TableAdapters;
using System.Text.RegularExpressions;
using System.IO;

namespace Conference
{
    public partial class _default : System.Web.UI.Page
    {
        countriesTableAdapter cta = new countriesTableAdapter();
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cdta = new conferencedataTableAdapter();
        string userID = "";
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack) {
                DropDownList1.DataSource = cta.GetData();
                DropDownList1.DataValueField = "c_id";
                DropDownList1.DataTextField = "Name";
                DropDownList1.DataBind();

                try
                {
                Label2.Text = cdta.GetData().Rows[0]["HomePageNotice"].ToString().Replace("\n", "<br/>");
                Label3.Text = cdta.GetData().Rows[0]["Requirements for Paper"].ToString().Replace("\n", "<br/>");
                Label4.Text = cdta.GetData().Rows[0]["ContactInfo"].ToString().Replace("\n", "<br/>");

               
                bool b = true;
                string isready = cdta.GetData().Rows[0]["ProgramCreated"].ToString();
                if (isready.Equals("0")) b = false;

                if (b)
                {
                    HyperLink1.Visible = true;
                    Label1.Visible = false;
                }
                else
                {
                    Label1.Visible = true;
                    HyperLink1.Visible = false;
                }
                }
                catch (Exception ex) { Response.Write(ex.Message); }
            }
        }
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            e.Authenticated = false;
            int rows = 0;
            rows = uta.GetDataForAuth(filter(Login1.UserName), filter(Login1.Password)).Rows.Count;
            e.Authenticated = (rows == 1);

            if (!e.Authenticated)
            {
                LoginStatus1_ModalPopupExtender.Show();
            }
            if (e.Authenticated)
            {
                string type = uta.GetDataByUserID(filter(Login1.UserName)).Rows[0]["type"].ToString();
                if (type.Equals("0"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName),true);
                    Response.Redirect("myprofile.aspx");
                }
                if (type.Equals("1"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("reviewer.aspx");
                }
                if (type.Equals("2"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("admin.aspx");
                }
                if (type.Equals("3"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("moder.aspx");
                }
            }
        }
        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoginStatus1_ModalPopupExtender.Hide();
        }
        protected void register(object sender, EventArgs e)
        {       
                string login = TextBox_2.Text;
                int n = uta.GetData().Rows.Count;
                bool b = false;
                
                for (int i = 0; i < n; i++) 
                {
                    if (uta.GetData().Rows[i]["user_id"].ToString().Equals(login)) 
                    {
                        b = true;
                        break;
                    }
                }
                
                if (!b)
                {
                    if (TextBox_2.Text != "" && TextBox_1.Text != "" && TextBox1.Text != "" && TextBox3.Text != "" && TextBox4.Text != "" && TextBox5.Text != "")
                    {
                        uta.InsertQuery(TextBox_2.Text, TextBox_1.Text, TextBox1.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, DropDownList1.SelectedItem.ToString(), "0", "", "", "", "1", "0", TextBox2.Text, CheckBox1.Checked.ToString());
                        sendEmailToAuthor(TextBox3.Text, TextBox1.Text);
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Registration complited');");
                        Response.Write("document.location.href='Login.aspx';");
                        Response.Write("</script>");
                    }
                    else 
                    {
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Please, fill all required fields');");
                        Response.Write("</script>");
                    }
                }
                else 
                {
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Login already exists. Please, choose another one');");
                    //Response.Write("document.location.href='default.aspx';");
                    Response.Write("</script>");
                }
        }
        void sendEmailToAuthor(string toEmail, string toName)
        {
            string c_name = cdta.GetData().Rows[0]["C_name"].ToString();
            string url = cdta.GetData().Rows[0]["URL"].ToString();
            string startdate = cdta.GetData().Rows[0]["startdate"].ToString().Substring(0,10);
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@ConfTitle@"; Alert[1].ParamValue = c_name;
            Alert[2].ParamName = "@date@"; Alert[2].ParamValue = startdate;
            Alert[3].ParamName = "@URL@"; Alert[3].ParamValue = url;
            Message.SendAlert("Registration", toEmail, "Registration", Alert).ToString();
        }
        protected string ReturnStatus(object val)
        {
            if (val != null)
            {

                if (val.ToString().Equals("0") || val.ToString().Equals("1")) return "<a style='Color:green'>Under evaluating process</a>";
                else if (val.ToString().Equals("2")) return "<a style='Color:blue'>Accepted</a>";
                else if (val.ToString().Equals("-1")) return "<a style='Color:Red'>Rejected</a>";
            }
            return "";
        }
        protected string ReturnDate(object val)
        {
            return val.ToString().Substring(0, 10);
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("submission.aspx");
        }
    }
}
