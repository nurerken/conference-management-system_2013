﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Papers.aspx.cs" Inherits="Conference.Papers" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="Admin.aspx">Managing abstracts</asp:HyperLink>
    <br/><br/>
    <ul class="nav nav-tabs">
      <li id = "new" class="active"><a href="#newsubs" data-toggle="tab" >New papers</a></li>
      <li id = "assed"><a href="#assignedsubs" data-toggle="tab">Reviewed papers</a></li>
      <li id = "reved"><a href="#reviewed" data-toggle="tab">Approved papers</a></li>
      <li id = "apped"><a href="#approved" data-toggle="tab">Presentations</a></li>      
      <li id = "schedule1"><a href="#schedule" data-toggle="tab">Schedule</a></li>      
      <li id = "Li1"><a href="#merge" data-toggle="tab">Journal Management</a></li>      
    </ul>
    <div class="tab-content" id="mytabcontent">
    <div class="tab-pane active" id="newsubs">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Abstract Average Mark">
                            <ItemTemplate>
                            <%# returnmark(Eval("avg_mark_abs"))%>
                            </ItemTemplate>
                         </asp:TemplateField>

                          <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>                
    </div>
    <div class="tab-pane" id="assignedsubs">
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>

        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                                       
                          <asp:BoundField DataField="rev1_mark_pp" HeaderText="Mark 1" ItemStyle-Width="100px"><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField="rev2_mark_pp" HeaderText="Mark 2" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:BoundField DataField= "avg_mark_pp" HeaderText="Average Mark" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>

                          <asp:TemplateField HeaderText="Average Mark(Equivalent)">
                            <ItemTemplate>
                            <%# returnmark(Eval("avg_mark_pp"))%>
                            </ItemTemplate>
                         </asp:TemplateField>

                          <asp:TemplateField HeaderText = "Approve" >
                                <ItemTemplate>
                                    <asp:Button ID="Button2" runat="server" Text="Approve" onclick="approve_btn_click" CommandArgument=<%# Eval("sub_id") %>/>
                                    <asp:Button ID="Button3" runat="server" Text="Cancel" onclick="cancel_btn_click" CommandArgument=<%# Eval("sub_id") %>/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="reviewed">
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView3" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                         <asp:TemplateField HeaderText="Average Mark Paper">
                            <ItemTemplate>
                            <%# returnmark(Eval("avg_mark_pp"))%>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                      <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="approved">
       <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView4" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                                                        
                            <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section"> 
                          </asp:BoundField>
                          
                          <asp:TemplateField HeaderText = "Presentation">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# "Presentations/"+ Eval("PPTPath") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                         <asp:TemplateField HeaderText = "Email" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("email") %>' NavigateUrl = 'mailto:<%# Eval("email") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div> 
     <div class="tab-pane" id="schedule">
        
        <asp:Button ID="Button1" runat="server" Text="Generate Schedule" OnClick="click1"/><br/>
        <asp:Button ID="Button6" runat="server" Text="Download Presentaions" OnClick="click21"/>
    </div>  

    <div class="tab-pane" id="merge">
        <asp:HyperLink ID="HyperLink1" runat="server" Visible="false">See Journal</asp:HyperLink>
        <asp:HyperLink ID="HyperLink4" runat="server">Download Journal</asp:HyperLink><br/>
        <asp:Button ID="Button4" runat="server" Text="Generate Journal" OnClick="journal" Visible="true"/>
    </div>          
    </div>    
</asp:Content>
