﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Submission.aspx.cs" Inherits="Conference.Submission" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
                
    <asp:Panel ID="Panel11" runat="server" BorderWidth="1">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        
        <table class="whitebgBorder" width="100%" border="0" cellpadding="4" cellspacing="0">             
            <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span><span class="style2"><strong>
                    About an article</strong></span><strong><span class="style3">
                    </span> </strong></span>
                </td>
            </tr>
            <tr>
               <td align="right">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Title:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Width="355px"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1" align="right">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Section:"></asp:Label>
                </td>
                <td>                
                   <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="true">  
                     
                </asp:RadioButtonList>  
                    <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>  
            
            <tr>
                <td class="style1" align="right">
                    
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server"  
                        Text="Please, chose some keywords via comma:" style="font-style: italic"></asp:Label>
                </td>
            </tr>

            <tr>
                <td class="style1" align="right">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Keywords:"></asp:Label>
                </td>
                <td>
                    <asp:CheckBoxList ID="ckeckboxlist1" runat="server" Font-Bold="True" ForeColor="#003300"  AutoPostBack="True"
                    onselectedindexchanged="ckeckboxlist1_SelectedIndexChanged" RepeatDirection="Vertical">
                        
                    </asp:CheckBoxList>

                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>    
            
             <tr>
            <td class="style1" align="right">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Is Virtual"></asp:Label>
                :</td>
            <td>
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </td>
            </tr>           
        <tr>
           <td>   
        </td>
        </tr>                      
         </table>
         </ContentTemplate>
       </asp:UpdatePanel>
       
       <table>
        <tr>
            <td class="style1" align="right">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Upload article"></asp:Label>
                :</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
         </tr> 
       </table>

        <br />       
        <p>
                 &nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:Button ID="Button1" runat="server" 
                            Text="Submit" 
                            onclick="Button1_Click" Font-Bold="True" Font-Size="Medium" 
                            Font-Strikeout="False" OnClientClick="return check();" 
                     Font-Underline="False"/>
        </p>
    </asp:Panel>   
                        
    <script type="text/javascript">

        function checkFileExtension() {
            var filePath = document.getElementById("ContentPlaceHolder1_FileUpload1").value;

            if (filePath.indexOf('.') == -1)
                return false;


            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = 'pdf';

            for (var i = 0; i < validExtensions.length; i++) {
                if (ext == validExtensions[i])
                    return true;
            }

            alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
            filePath.value = "";
            return false;
        }

        function check() {
            //3,6,2,7
            var t1 = document.getElementById("ContentPlaceHolder1_TextBox1").value; //title
            
            if (t1.toString() == "") {
                alert("Field Title is required");
                return false;
            }
            checkFileExtension();
            
            return true;
        }
    </script>
</asp:Content>

