﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Reviewer.aspx.cs" Inherits="Conference.Reviewer" %>
<%@ Import Namespace="Conference" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h3>
                <a href="editProfile.aspx">Edit Profile</a>
    </h3>
    <ul class="nav nav-tabs">
      <li id = "new" class="active"><a href="#newsubs" data-toggle="tab" >New abstracts</a></li>
      <li id = "assed"><a href="#assignedsubs" data-toggle="tab">Checked abstracts</a></li>
      <li id = "reved"><a href="#reviewed" data-toggle="tab">New Papers</a></li>
      <li id = "apped"><a href="#approved" data-toggle="tab">Checked Papers</a></li>            
      <li id = "Li1"><a href="#all" data-toggle="tab">All submissions for me</a></li>            
      <li id = "Li2"><a href="#make" data-toggle="tab">Submission</a></li>            
    </ul>

    <div class="tab-content" id="mytabcontent">
    <div class="tab-pane active" id="newsubs">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# Eval("sub_id")  %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>
                         
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>        
    </div>
    <div class="tab-pane" id="assignedsubs">
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>     
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="reviewed">
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView3" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click3" 
                                    CommandArgument=<%# Eval("sub_id")  %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section" ItemStyle-Width="100px" ><ItemStyle Width="100px"></ItemStyle>
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>                      
                         </Columns>
                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="approved">
       <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView4" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section"> 
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>

                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>    
    <div class="tab-pane" id="all">
       <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
        <asp:GridView ID="GridView6" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>  
                         <asp:TemplateField HeaderText = "Title" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton1"                                     
                                    Text='<%# Eval("Title") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.SubmissionsRow)(((DataRowView)Container.DataItem).Row)).sub_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="SubmissionTopicArea" HeaderText="Section"> 
                          </asp:BoundField>
                                                   
                          <asp:TemplateField HeaderText="Submission Date">
                            <ItemTemplate>
                            <%# ReturnDate(Eval("Sub_date")) %>
                            </ItemTemplate>
                         </asp:TemplateField>

                         <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                            <%# ReturnStatus(Eval("status"))%>
                            </ItemTemplate>
                         </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div> 
    <div class="tab-pane" id="make">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Submission.aspx">Make Submission</asp:HyperLink>
    </div> 
    </div>

</asp:Content>
