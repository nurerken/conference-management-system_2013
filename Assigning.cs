﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using Conference.DataSet1TableAdapters;

namespace Conference
{
    public class Assigning
    {
        usersTableAdapter uta = new usersTableAdapter();
        SubmissionsTableAdapter sta = new SubmissionsTableAdapter();
        conferencedataTableAdapter cta = new conferencedataTableAdapter();
        static double[][] K;
        static double[][] K2;
        int R, P;
        Paper[] papers;
        Reviewer_[] reviewers;
        Reviewer_[] reviewers2;
        bool[,] pairs;
        bool[,] pairs2;
        double cost1, cost2;
        string topic;

        public void assign() 
        {
            //for each topic
            //create reviewers list
            //create papers list
            //Build K matrix
            //Call Hungarian algorithm twice
            //Write to DB
            topic = "Development and operation of oil and gas fields";
            createReviewersList(topic);
            createPapersList(topic);
            buildKmatrix();
            HungarianAlgorithm();
            writeToDB();
            eliminate();

            topic = "Technology of hydrocarbon and mineral resources and environmental challenges of oil and gas sector";
            createReviewersList(topic);
            createPapersList(topic);
            buildKmatrix();
            HungarianAlgorithm();
            writeToDB();
            eliminate();

            topic = "Mechanics, mathematical modeling and information technologies in the oil and gas sector";
            createReviewersList(topic);
            createPapersList(topic);
            buildKmatrix();
            HungarianAlgorithm();
            writeToDB();
            eliminate();

            topic = "Socio-economic problems and training issues for the oil and gas sector";
            createReviewersList(topic);
            createPapersList(topic);
            buildKmatrix();
            HungarianAlgorithm();
            writeToDB();
            eliminate();
        }
        void createReviewersList(string topic) 
        {
            R = uta.GetDataByTypeAndTopic("1",topic).Rows.Count;
            reviewers = new Reviewer_[R];
            
            for (int i = 0; i < R; i++)
            {
                string userID = uta.GetDataByTypeAndTopic("1", topic).Rows[i]["user_id"].ToString();
                string[] keywords = uta.GetDataByTypeAndTopic("1", topic).Rows[i]["keyword"].ToString().Split(',');
                reviewers[i] = new Reviewer_(userID,keywords);
            }
            System.Diagnostics.Debug.WriteLine("R = "+R);
        }
        void createPapersList(string topic)
        {
            P = sta.GetDataByTopicArea(topic).Rows.Count;
            papers = new Paper[P];
            pairs = new bool[P, P];
            pairs2 = new bool[P, P];

            for (int i = 0; i < P; i++)
            {
                string PaperID = sta.GetDataByTopicArea(topic).Rows[i]["sub_id"].ToString();
                string[] keywords = sta.GetDataByTopicArea(topic).Rows[i]["submissionKeyWords"].ToString().Split(',');
                papers[i] = new Paper(PaperID, keywords); 
            }
            
        }
        void buildKmatrix() 
        {
            K = RectangularArrays.ReturnRectangularDoubleArray(P, P);
            K2 = RectangularArrays.ReturnRectangularDoubleArray(P, P);
            
            int cnt = 0;
            int i;
            reviewers2 = new Reviewer_[P];
            while(cnt<P){
                for (i = 0; i < reviewers.Length; i++) {
                    for (int j = 0; j < papers.Length; j++)
                    {
                        if (i + cnt < P)
                        {
                            K[i + cnt][j] = Math.Round(SF(papers[j], reviewers[i]), 4);
                            K2[i + cnt][j] = K[i + cnt][j];
                            reviewers2[i + cnt] = reviewers[i];
                        }                        
                    }                    
                }
                cnt+=R;
            }            
        }
        void HungarianAlgorithm() 
        {
            cost1 = Hungarian.hgAlgorithm(K, pairs, pairs2, 1);
            updateK();
            cost2 = Hungarian.hgAlgorithm(K, pairs,pairs2,2);            
        }
        void eliminate() 
        {
            string rev1_id, rev2_id;
            for (int i = 0; i < P; i++) 
            {
                rev1_id = sta.GetDataByTopicArea(topic).Rows[i]["rev1_id"].ToString();
                rev2_id = sta.GetDataByTopicArea(topic).Rows[i]["rev2_id"].ToString();
                
                if (rev1_id.Equals(rev2_id)) 
                {
                    int[] fullness = new int[R];
                    double max = -1;

                    for (int j = 0; j < R; j++)
                    {
                        fullness[j] = Int32.Parse(uta.GetDataByUserID(reviewers[j].ID).Rows[0]["rev_number"].ToString());
                        if(fullness[j]>max) max = fullness[j];
                    }

                    bool b = false;
                    double MaxSF = K2[0][i] * (max - fullness[0]); ;//-1;
                    double MaxSF2 = K2[0][i] * (max - fullness[0]); ;//-1;
                    int maxJ = 0;
                    double maxSFF = -1;
                    int maxSFJ = 0;
                    double temp = 0;

                    for (int j = 0; j < R; j++) 
                    {
                        if (!reviewers[j].ID.Equals(rev1_id))
                        {
                            MaxSF = K2[j][i] * (max - fullness[j]);
                            if (MaxSF > MaxSF2)
                            { 
                                MaxSF2 = MaxSF;
                                maxJ = j;
                                b = true;
                            }

                            temp = K2[j][i];
                            if (temp > maxSFF) 
                            {
                                maxSFF = temp;
                                maxSFJ = j;
                            }                            
                        }
                    }
                    if (b)
                    {
                        sta.UpdateReviewer2ID(reviewers[maxJ].ID, Int32.Parse(sta.GetDataByRevIDS(rev1_id, rev2_id).Rows[0]["sub_id"].ToString()));
                        uta.UpdateRevNumber(reviewers[maxJ].ID);
                        pairs2[maxJ, i] = true;
                    }
                    else 
                    {
                        sta.UpdateReviewer2ID(reviewers[maxSFJ].ID, Int32.Parse(sta.GetDataByRevIDS(rev1_id, rev2_id).Rows[0]["sub_id"].ToString()));
                        uta.UpdateRevNumber(reviewers[maxSFJ].ID);
                        pairs2[maxSFJ, i] = true;
                    }

                }
            }

            //send email
            for (int j = 0; j < reviewers.Length; j++)
            {
                bool b = false;
                for (int i = 0; i < papers.Length; i++)
                {
                    if (pairs[j, i] || pairs2[j, i])
                    {
                        b = true; break;
                    }
                }
                if (b)
                {
                    string email = uta.GetDataByUserID(reviewers[j].ID).Rows[0]["email"].ToString();
                    string FIO = uta.GetDataByUserID(reviewers[j].ID).Rows[0]["FIO"].ToString();
                    sendEmailToReviewer1(email, FIO);
                }
            }
        }
        void updateK() 
        {
            for (int i = 0; i < P; i++)
            {
                string id = "";
                for (int j = 0; j < P; j++)
                {
                    if (pairs[j, i]) { id = reviewers2[j].ID; break; }
                }

                for (int j = 0; j < P; j++)
                {
                    if (reviewers2[j].ID.Equals(id)) K[j][i] = -10;//double.NegativeInfinity+1;
                }
            }   
        }
        void writeToDB() 
        {
            for (int i = 0; i < P; i++) 
            {
                int cnt = 0;
                string sub_id = "";
                int jj = 0;
                int jj2 = 0;

                for (int j = 0; j < P; j++)
                {
                    if (pairs[j,i]) 
                    {
                        jj = j;
                        string rev_id = reviewers2[j].ID;
                        sub_id = papers[i].ID;
                        sta.UpdateReviewer1ID(rev_id, Int32.Parse(sub_id));
                        //uta.UpdateRevNumber(rev_id);
                        cnt++;
                    }
                    if (pairs2[j, i])
                    {
                        jj2 = j;
                        string rev_id = reviewers2[j].ID;
                        sub_id = papers[i].ID;                        
                        sta.UpdateReviewer2ID(rev_id, Int32.Parse(sub_id));
                        //uta.UpdateRevNumber(rev_id);
                        cnt++;
                    }

                    if (jj == jj2) uta.UpdateRevNumber(reviewers2[jj].ID);
                    else
                    {
                        uta.UpdateRevNumber(reviewers2[jj].ID);
                        uta.UpdateRevNumber(reviewers2[jj2].ID);
                    }

                    if (cnt == 2) { sta.UpdateStatus("1", Int32.Parse(sub_id)); break; }
                }    
            }

            /*
            System.Diagnostics.Debug.WriteLine("--------------");
            for (int i = 0; i < P; i++)
            {
                for (int j = 0; j < P; j++)
                {
                    System.Diagnostics.Debug.Write(pairs[i,j] + " ");
                }
                System.Diagnostics.Debug.WriteLine("");
            }
            System.Diagnostics.Debug.WriteLine("-----");
            for (int i = 0; i < P; i++)
            {
                for (int j = 0; j < P; j++)
                {
                    System.Diagnostics.Debug.Write(pairs2[i, j] + " ");
                }
                System.Diagnostics.Debug.WriteLine("");
            }
            System.Diagnostics.Debug.WriteLine("--------------");
             */
            
        }
        double SF(Paper p, Reviewer_ r)
        {
            ArrayList al = new ArrayList();

            int cnt = 0;

            for (int i = 0; i < p.keywords.Length; i++)
            {
                al.Add(p.keywords[i]);
            }

            for (int i = 0; i < r.keywords.Length; i++)
            {
                if (!al.Contains(r.keywords[i])) al.Add(r.keywords[i]);
            }

            double union = al.Count;

            bool b;
            for (int i = 0; i < p.keywords.Length; i++)
            {
                b = false;
                for (int j = 0; j < r.keywords.Length; j++)
                {
                    if (p.keywords[i].Equals(r.keywords[j]))
                    {
                        b = true;
                        //break;
                    }
                }

                if (b)
                {
                    cnt++;
                }
            }

            if (sta.GetDataBySubID(Int32.Parse(p.ID)).Rows[0]["Author_ID"].ToString().Equals(r.ID)) return Double.NegativeInfinity;
            return cnt * 1.0 / union;
        }
        public static void set(double[][] arr, int i, int j, double v)
        {
            arr[i][j] = v;
        }
        void sendEmailToReviewer1(string toEmail, string toName)
        {
            string confname = cta.GetData().Rows[0]["c_name"].ToString();
            string url = cta.GetData().Rows[0]["url"].ToString();
            MailAlert Message = new MailAlert();
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[3];
            Alert[0].ParamName = "@Name@"; Alert[0].ParamValue = toName;
            Alert[1].ParamName = "@ConfTitle@"; Alert[1].ParamValue = confname;
            Alert[2].ParamName = "@url@"; Alert[2].ParamValue = url;
            Message.SendAlert("ToReviewerAbstract", toEmail, "New submission", Alert).ToString();
        }
    }

   
}